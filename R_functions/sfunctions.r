#sfunctions.r
#Contains small function used in a variety of situations


##### 1. functions to transform data #####
# the most recent version relies on the functions from the flowCore package, 
# but a lot of analuyses were done using the following functions
# and they still appear in some instances (e.g. transforming color scales)

logicle <- function(x, w = 1, Top= 10000, m = 4) {
  #w <- 2*p * log(p) / (p+1)
  p <- ifelse(w == 0, 1,w / (2 * (lambert_W0(0.5 * exp(-w/2) * w))))
  Top * exp(-m+w) * (exp(x-w) - p^2 * exp(-(x-w)/p) + p^2 - 1)
}

ilogicle <- function(d, m = 4, outliers = NULL, r =NULL) {
#   range <- seq(0, log10(Top), by=0.01)
  #also remove some outliers
  if (is.null(outliers)) outliers <- quantile(d, probs=0.99)
  #print(outliers)
  d[d>outliers] <- outliers
  Top <- outliers
  
  range <- seq(0,m, length.out=10000)
  negatives <- d[d<0]
  if (is.null(r)) r <- abs(quantile(negatives, 0.05))
  #print(r)
  w <- ifelse(is.na(r), 0, (m - log(Top / abs(r)))/2)
  w <- ifelse(w <0 , 0,w)
  
  l <- logicle(range, w = w, Top, m=m)

  trans <- sapply(d, function(x) range[which.min(abs(l - x))])
  trans

}

oldtransform <- function(x, how, outliers=NULL, r= NULL) {
  switch(how,
         log = ifelse(x > 0, log(x), 0),
         biexponential = ilogicle(x, outliers=outliers, r= r),
         logicle = ilogicle(x, outliers=outliers, r= r),
         x
        )
}


and_connect <- function(df) {
  cnames <- colnames(df)
  apply(df, 1, function(x) {
    if (sum(x) == 0 ) return("other") else {
      paste(cnames[x>0],collapse="&")
    }
  })
}


matches <- function(df1, df2) {
  res <- T
  for(f1 in colnames(df1)) {
    res <- res & identical(df1[,f1], df2[,f1])
  }
  res
}

#### 2. function to import gene lists #####
#translates input (gene name, gene name w/ wildcard, ENSEMBL gene ID, entrez gene ID) into colnames of the uv frame.
checkInput <- function(x, call = NA, martf, uv) {
  #browser()
  
  if (is.na(call)) {
    call <- paste0(LETTERS[sample(1:26,10,replace=T)],collapse="")
    father <- T
    assign(call, x, ".GlobalEnv")
    traversed <- x
  } else {
    traversed <- get(call,".GlobalEnv")
    father <- F
    assign(call, c(x,traversed), ".GlobalEnv")
  }
  if (is.na(x)) {
    return(NULL)
  }
  if (x == "") {
    return(NULL)
  } else if (grepl("FACS",x)) {
    res <- x
    if(!(res %in% colnames(uv))) {if (father) rm(list=call, envir=.GlobalEnv); return(NULL) }
  } else if(grepl("^ENS|FBgn",x, ignore.case=T, perl =T)) {
    
    ens <- x
    hgnc <- unique( martf[martf$ensembl_gene_id == ens,2] )
    res <- paste(hgnc, " (", ens, ")",sep="")
      if (length(res) > 1) warning("Non unique Gene name!")
    if(!(res %in% colnames(uv))) {if (father) rm(list=call, envir=.GlobalEnv); return(NULL) }
  } else if(grepl("^\\d+$",x, ignore.case=T, perl =T)) {
    s <- na.omit(martf)
    ens <- unique(s$ensembl_gene_id[s$entrezgene == x])
    hgnc <- unique( s[s$ensembl_gene_id == ens,2] )
    res <- paste(hgnc, " (", ens, ")",sep="")
    if(!(res %in% colnames(uv))) {if (father) rm(list=call, envir=.GlobalEnv); return(NULL) }
    if (length(res) > 1) warning("Non unique Gene name!")
  } else if (substr(x, nchar(x), nchar(x)) == "*") {
    matches <- grep(paste("^",substr(x,1,nchar(x)-1),sep=""), martf[,2],ignore.case=T)
    res <- unlist(sapply(martf[matches,2], checkInput, martf=martf, uv=uv))
  } else if (substr(x, 1,3) == "GO:") {
    matches <- ENSEMBL2GO$ENSEMBL[ENSEMBL2GO$GO == x]
    matches <- c(matches, GOCHILDREN[[x]])
    matches <- matches[!(matches %in% traversed)]
    res <- unlist(sapply(matches, checkInput, call = call, martf=martf, uv=uv))
  }else {
    hgnc <- toupper(x)
    ens <- unique(martf$ensembl_gene_id[martf[,2] == hgnc])
    res <- paste(hgnc, " (", ens, ")",sep="")
    if(!(res %in% colnames(uv))) {if (father) rm(list=call, envir=.GlobalEnv); return(NULL) }
    
  }
  
  
  if(length(res) > 0 ) {
    if (father) rm(list=call, envir=.GlobalEnv)
    return(unique(res)) 
    } else {
    cat("Not found: ", x, "\n")
   if (father) rm(list=call, envir=.GlobalEnv); return(NULL)
  }
}

removeSpaces <- function(s) {
  gsub("\\s",replacement="",s)
}

mapVariable2Channel <- function(id) { #legacy of earlier code
  id
}

##### 3. functions to manage system workload #####

n.cores <- function() {
  command <-paste("top -bn1 |",PERLCOMMAND,"-ne 'if (/^\\d+/) {@spl = split(/\\s+/, $_); print $spl[8] . \"\\n\"}'")
  perl <- pipe(command,open="r")
  usage <- readLines(perl)
  close(perl)
  cores_used <- ceiling(sum(as.numeric(usage))/100)
  use <- floor((MAXCORES - cores_used) *3/4)
  cat("Working on ", use, " cores \n")
  return(use)
}

estimateSizeFactorsForMatrix <- function(counts, locfunc = stats::median, geoMeans, controlGenes) {
    if (missing(geoMeans)) {
        incomingGeoMeans <- FALSE
        loggeomeans <- rowMeans(log(counts))
    }
    else {
        incomingGeoMeans <- TRUE
        if (length(geoMeans) != nrow(counts)) {
            stop("geoMeans should be as long as the number of rows of counts")
        }
        loggeomeans <- log(geoMeans)
    }
    if (all(is.infinite(loggeomeans))) {
        stop("every gene contains at least one zero, cannot compute log geometric means")
    }
    sf <- if (missing(controlGenes)) {
        apply(counts, 2, function(cnts) {
            exp(locfunc((log(cnts) - loggeomeans)[is.finite(loggeomeans) & 
                cnts > 0]))
        })
    }
    else {
        if (!(is.numeric(controlGenes) | is.logical(controlGenes))) {
            stop("controlGenes should be either a numeric or logical vector")
        }
        loggeomeansSub <- loggeomeans[controlGenes]
        apply(counts[controlGenes, , drop = FALSE], 2, function(cnts) {
            exp(locfunc((log(cnts) - loggeomeansSub)[is.finite(loggeomeansSub) & 
                cnts > 0]))
        })
    }
    if (incomingGeoMeans) {
        sf <- sf/exp(mean(log(sf)))
    }
    sf
}

getGOrilla <- function(hits, rest, db = "proc", species) {
  species <- switch(species,
                    "human" = "HOMO_SAPIENS",
                    "mouse" = "MUS_MUSCULUS",
                    "fly" = "DROSOPHILA_MELANOGASTER",
                    species)
  hits <- paste(hits, collapse = "\n")
  rest <- paste(rest, collapse = "\n")
  gorilla <- POST(url = "http://cbl-gorilla.cs.technion.ac.il/servlet/GOrilla", body = list(
    "application" = "gorilla", "species" = species, "run_mode" = "hg" , 
    "target_set" = hits , "background_set" = rest , 
    "db" = db , "pvalue_thresh" = 0.001, "fast_mode" = "on"), encode= "form")
  
  gorilla$url

}

# bitsToInt<-function(x) {
#   packBits(rev(c(rep(FALSE, 32-length(x)%%32), as.logical(x))), "integer")
# }
# 
# compress_bin <- function(binary ) {
#   code <- c(LETTERS,letters,0:9,"-","_")
#   current <-c()
#   out <- c()
#   i <- 1
#   while (length(binary)>0) {
#     current <- c(current, binary[1])
#     binary <- binary[-1]
#     if(length(current) == 6) {
#       out <- c(out, code[bitsToInt(current)+1])
#       current <- c()
#     }
#     
#   }
#   if(length(current)>0) out <- c(out, code[bitsToInt(current)])
#   paste(out,collapse="")
# }
