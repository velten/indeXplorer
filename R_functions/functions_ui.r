openGenecards <- function(data, location, session) {
  if (grepl("ENSG",data$name)){
    gene <- strsplit(data$name, "[\\(\\)]")[[1]][1]
    session$sendCustomMessage("OpenGencards", gene)
  }
}