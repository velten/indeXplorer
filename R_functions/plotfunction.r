#This is the central piece of the scatter plot module. It consists of three parts: functions, an observers, and reactive elements


##### 1. functions #####
#the function plotfunction draws the plots from the scatter plots module where required (see below)

#it depends on several helper functions to read the gating hierarchy:
#get all variables that the gate, or any of its parents, was defined on
getparams <- function(filter) {
  if (class(filter) %in% c("intersectFilter","unionFilter" )) unlist(lapply(filter@filters, getparams)) else unlist(lapply(filter@filter@parameters, function(x) x@parameters))
}

#get the type of transformation that a gate was defined on
gettransform <- function(transform) {
  stringrep <- format(transform@f)
  if(any(grepl("logicle", stringrep))) {
    return("logicle")
  } else if (any(grepl("log", stringrep))) {
    return("log")
  } else if (any(grepl("asinh", stringrep))) {
    return("asinh")
  } else return("linear")
}

#create a summary of a gate that's easier to read from than the flowCore filter object
gettopparams <- function(filter) {
  if (class(filter)  %in% c("intersectFilter","unionFilter" )) gettopparams(filter@filters[[2]]) else {
    names <- unlist(getparams(filter))
    transforms <- sapply(filter@transforms@transforms, gettransform)
    class <- class(filter@filter)
    if(class(filter@filter) == "polygonGate") {

      coords <- as.data.frame(filter@filter@boundaries)
      colnames(coords) <- c("x","y")
      } else { 
       coords <-  data.frame(xmin =filter@filter@min[1], xmax =filter@filter@max[1],
                             ymin =filter@filter@min[2], ymax =filter@filter@max[2])
      }
    list(names=names,transforms=transforms,coords=coords, type = class(filter@filter))
  }
}

#find out which parent the gate has
getParent <- function(filter) {
  if (class(filter)  == "intersectFilter") {
    name <- filter@filters[[1]]@filterId
    name <- gsub(" and ", "&", name)
    name <- gsub(" or ", "|", name)
    return(name)
  } else return("all")
}

plotfunction <- function(pid) {
  gpp <- isolate(globalData$plots)
  ggg <- isolate(globalData$Gates)
  ggpop <- isolate(globalData$Populations)
 #browser()  
if (nrow(gpp) >= pid) {
    xvar <- gpp$x[gpp$id == pid]
    yvar <- gpp$y[gpp$id == pid]
    pltype <- gpp$type[gpp$id == pid]
    #define variables needed to draw the plot
    if (gpp$color_type[gpp$id == pid] == "univariate") {
      usevars <- c(xvar,yvar, gpp$color_variable[gpp$id == pid])
    } else usevars <- c(xvar,yvar)

    colScoreID <- ""

    #first thing to check: what gates to apply
    display <- strsplit(gpp$todisplay[gpp$id == pid],split="_")[[1]]
    hide <- strsplit(gpp$tohide[gpp$id == pid],split="_")[[1]]

    #create the gate that applies (if multiple gates are combined in one plot)
    if (display[1] != "all") {
      filter_to_apply <- ggg[[pltype]][[ which(getGateNames(pltype) == display[1]) ]]
      if(length(display) > 1) {
        for (i in 2:length(display)) {
        if (gpp$gatingMode[gpp$id == pid] == "AND") filter_to_apply <- filter_to_apply & ggg[[pltype]][[ which(getGateNames(pltype) == display[i] ) ]]
        if (gpp$gatingMode[gpp$id == pid] == "OR") filter_to_apply <- filter_to_apply | ggg[[pltype]][[ which(getGateNames(pltype) == display[i] ) ]]
        }
      }
      
      #variables required to draw the plot are those defined earlier, plus those required for filtering
      usevars <- unique(c(usevars, unname(getparams(filter_to_apply))))
      #if gates are defined in PCA space, we need to access the PCA data
      if (any(grepl("^PC[[:digit:]]", usevars) | grepl("^GL_", usevars) )) univariates[[pltype]] <- flowFrame(as.matrix(cbind(univariates[[pltype]]@exprs, do.call(cbind,globalData$projections))))
      
      #remove all variables that are not needed for drawing the plot - otherwise the filter operation of flowCore will be slow
      #browser()    
      univariates[[pltype]] <- univariates[[pltype]][,usevars]
      #filter cells by gating
      f <- filter(univariates[[pltype]], filter_to_apply)
      use <- f@subSet
      
      #check if a certain population is to be removed from display
      #the point of this is to be able to show e.g. only one population from a hierarchical clustering against all background events
      if (length(hide)==1) use <- use & (! ggpop[[pltype]][,hide]) else if (length(hide) > 1) use <- use & (! apply(ggpop[[pltype]][,hide], 1, any))
      univariates[[pltype]] <- univariates[[pltype]][use,]
      ggpop[[pltype]] <- ggpop[[pltype]][use,]
      usecs <- lapply(globalData$colorScores, function(x) x[use])
      names(usecs) <- names(globalData$colorScores)
      
      #if applicable, also filter background events
      if (substr(xvar,1,5) == "FACS_" & substr(yvar,1,5) == "FACS_" & gpp$showDensity[pid]) complete_facs[[pltype]] <- Subset(complete_facs[[pltype]], filter_to_apply)
    } else {
       
      #if no gates apply, just remove the cells which are to be hidden from display (see above, rarely used)
      #browser()
      if (any(grepl("^PC[[:digit:]]", usevars) | grepl("^GL_", usevars) )) univariates[[pltype]] <- flowFrame(as.matrix(cbind(univariates[[pltype]]@exprs, do.call(cbind,globalData$projections))))
      if (length(hide)==1) use <- (! ggpop[[pltype]][,hide]) else if (length(hide) > 1) use <- (! apply(ggpop[[pltype]][,hide], 1, any)) else use <- rep(T, nrow(univariates[[pltype]]))
      univariates[[pltype]] <- univariates[[pltype]][use,]
      ggpop[[pltype]] <- ggpop[[pltype]][use,]
      usecs <- lapply(globalData$colorScores, function(x) x[use])
      names(usecs) <- names(globalData$colorScores)
      univariates[[pltype]] <- univariates[[pltype]][,usevars]
      usecs <- globalData$colorScores
    }
    
    

      if (substr(xvar,1,5) == "FACS_") {
        #for flow cytometric data transform the data as requested by user
      xscale <- gpp$x_scale[gpp$id == pid]
      xt <- switch(xscale,
                   logicle = logicleTrans[[pltype]]@transforms[[mapVariable2Channel(xvar)]]@f,
                   log = function(x) ifelse(x > 0, log(x), 0),
                   linear = function(x) x,
                   asinh = asinh
      )
    #browser()  
  
    #if (xvar == "FACS_CD34_APC"  & xscale == "logicle") xt <- logicleTransform(transformationId="facstrans", a = 0, w = 1.2, t = 53425.28, m = 4.5)@.Data
    #if (xvar == "FACS_CD45RA_FITC" & xscale == "logicle") xt <- logicleTransform(transformationId="facstrans", a = 0, w = 1.4, t = 53425.28, m = 4.5)@.Data
    #if (xvar == "FACS_CD10_VL" & xscale == "logicle") xt <- logicleTransform(transformationId="facstrans", a = 0, w = 1.1, t = 53425.28, m = 4.5)@.Data
      
     trans <- transformList(from = mapVariable2Channel(xvar),
          tfun = list(xt),
          transformationId = "facstrans")
     
     
     
     
     myfacst <- transform(univariates[[pltype]], trans)
     x <- myfacst@exprs[,mapVariable2Channel(xvar)]
     if (gpp$showDensity[pid]) facst <- transform(complete_facs[[pltype]], trans)
     
     #also, create a scale (ticks for x axis)
     if (gpp$showDensity[pid]) x_scale_from <- min(complete_facs[[pltype]]@exprs[,mapVariable2Channel(xvar)]) else x_scale_from <- min(univariates[[pltype]]@exprs[,mapVariable2Channel(xvar)])
     if (gpp$showDensity[pid]) x_scale_to <- max(complete_facs[[pltype]]@exprs[,mapVariable2Channel(xvar)]) else x_scale_to <- max(univariates[[pltype]]@exprs[,mapVariable2Channel(xvar)])
     x_scale_from <- ifelse(x_scale_from ==0, 0, round(sign(x_scale_from) * log10(abs(x_scale_from))))
     x_scale_to <- round(sign(x_scale_to) * log10(abs(x_scale_to)))
     whathefuck <- function(x) sign(x) * 10^abs(x)
     x_labels <- matrix(whathefuck(x_scale_from:x_scale_to),ncol=1)
     colnames(x_labels) <- mapVariable2Channel(xvar)
     x_labels <- flowFrame(x_labels)
     x_breaks <- transform(x_labels,trans)
       
      
      } else {
      x <- univariates[[pltype]]@exprs[, xvar]
      xscale <- "linear"
    }


      if (substr(yvar,1,5) == "FACS_") {
      yscale <- gpp$y_scale[gpp$id == pid]
      yt <- switch(yscale,
                   logicle = logicleTrans[[pltype]]@transforms[[mapVariable2Channel(yvar)]]@f,
                   log = function(x) ifelse(x > 0, log(x), 0),
                   linear = function(x) x,
                   asinh = asinh
      )
      
      
      #if (yvar == "FACS_cd38" & yscale == "logicle") yt <- logicleTransform(transformationId="facstrans", a = 0, w = 1.1, t = 63425.28, m = 4.5)@.Data
      #if (yvar == "FACS_CD135_PE" & yscale == "logicle") yt <- logicleTransform(transformationId="facstrans", a = 0, w = 1.2, t = 63425.28, m = 4.5)@.Data
      
      
      trans <- transformList(from = mapVariable2Channel(yvar),
       tfun = list(yt),
      transformationId = "facstrans")
      
      myfacst <- transform(univariates[[pltype]], trans)
      y <- myfacst@exprs[,mapVariable2Channel(yvar)]
      if (exists("facst")) facst <- transform(facst, trans)
      
      #also, create a scale (ticks for x axis)
     if (gpp$showDensity[pid]) y_scale_from <- min(complete_facs[[pltype]]@exprs[,mapVariable2Channel(yvar)]) else y_scale_from <- min(univariates[[pltype]]@exprs[,mapVariable2Channel(yvar)])
     if (gpp$showDensity[pid]) y_scale_to <- max(complete_facs[[pltype]]@exprs[,mapVariable2Channel(yvar)]) else y_scale_to <- max(univariates[[pltype]]@exprs[,mapVariable2Channel(yvar)])
      y_scale_from <- ifelse(y_scale_from ==0, 0, round(sign(y_scale_from) * log10(abs(y_scale_from))))
      y_scale_to <- round(sign(y_scale_to) * log10(abs(y_scale_to)))
      whathefuck <- function(x) sign(x) * 10^abs(x)
      y_labels <- matrix(whathefuck(y_scale_from:y_scale_to),ncol=1)
      colnames(y_labels) <- mapVariable2Channel(yvar)
      y_labels <- flowFrame(y_labels)
      y_breaks <- transform(y_labels,trans)
      
      
    } else {
      y <- univariates[[pltype]]@exprs[, yvar]
      yscale <- "linear"
    }
    
    xlab <- paste(xvar , " (", xscale ,")",sep="") #create labels
    ylab <- paste(yvar , " (", yscale ,")",sep="")
  
  #create color variable, depending on color type
  if (gpp$color_type[gpp$id == pid] == "univariate") {
    
    col <- univariates[[pltype]]@exprs[, gpp$color_variable[gpp$id == pid]]
    clab <- gpp$color_variable[gpp$id == pid]
    if (grepl("ENSG", clab)) clab <- strsplit(clab, "[ \\(\\)]")[[1]][1] 
    if (gpp$color_variable[gpp$id == pid] != "linear") col <- oldtransform(col, how=  gpp$color_scale[gpp$id == pid])
    cscale <- "linear"
    
  } else if (gpp$color_type[gpp$id == pid] == "population") {
    use_populations <- strsplit(gpp$color_variable[gpp$id == pid] , "_")[[1]]
    if (length(use_populations) == 0) col <- "none" else if (length(use_populations) == 1) { 
      col <- ifelse(ggpop[[pltype]][,use_populations], use_populations, "other") 
    }else {
      col <- and_connect(ggpop[[pltype]][,use_populations])
    }
    clab <- "Populations"
    cscale <- "discrete"
  } else if (gpp$color_type[gpp$id == pid] == "gene list") {
    colScoreID <- removeSpaces(paste(gpp[gpp$id == pid, c("color_variable", "color_scale")],collapse="_"))
    col <- usecs[[colScoreID]]
    if (is.null(col)) col <- 0
    clab <- gpp[gpp$id == pid, "color_variable"]
    cscale <- "linear"
  } else if (gpp$color_type[gpp$id == pid] == "PCA") {
    colScoreID <- paste("PC",gpp$color_variable[gpp$id == pid],sep="")
    col <- usecs[[colScoreID]]
    #print(head(col))
    clab <- colScoreID
    cscale <- "linear"
  } else {
    col <- "none"
    clab <- ""
    cscale <- "none"
  }
    
    ## add gatelines #
    #what's the parent of the current plot? Also used for labeling the plot
    parent_label <- paste(display,collapse = ifelse(gpp$gatingMode[gpp$id == pid] == "AND","&","|"))
    if (length(ggg[[pltype]]) > 0) {  

    allparams <- lapply(ggg[[pltype]], gettopparams)
    for (i in 1:length(ggg[[pltype]])) {
      allparams[[i]]$name <- getGateNames(pltype)[i]
      allparams[[i]]$parent <- getParent(ggg[[pltype]][[i]])
    }
    usedata_rect <- do.call(rbind,lapply(allparams, function(x) {
      n <- x[["names"]];
      t <- x[["transforms"]]; 
      use <- all(t %in% c(xscale,yscale)) & all(n %in% c(xvar,yvar)) & x[["parent"]] == parent_label;
      if (use & x[["type"]] == "rectangleGate") {
        out <- x[["coords"]];
        out$name <- x[["name"]]
        return(out)
      } else return(NULL)
    }))
    usedata_poly <- do.call(rbind,lapply(allparams, function(x) {
      n <- x[["names"]];
      t <- x[["transforms"]]; 
      use <- all(t %in% c(xscale,yscale)) & all(n %in% c(xvar,yvar))& x[["parent"]] == parent_label;
      if (use & x[["type"]] == "polygonGate") {
        out <- x[["coords"]];
        out$name <- x[["name"]]
        return(out)
      } else return(NULL)
    }))
    labelf_poly <- ddply(.data = usedata_poly, .variables = "name", .fun = summarise, x = min(x), y = min(y))
    } else { usedata_poly <- NULL; usedata_rect <- NULL}
    
    labelf_parent <- data.frame(x = min(x), y= max(y), name=parent_label)
    
    
    plf <- data.frame(x=x,y=y,c=col)
    
    if(pltype=="scCulture") plf <- data.frame(x=x,y=y,c=col)
#     find_hull <- function(df) df[chull(df$x, df$y), ]
#     hulls <- ddply(plf, "c", find_hull)
    

  
    if(substr(xvar,1,5) == "FACS_" & substr(yvar,1,5) == "FACS_") {
      #create plot with density of all FACS events in background

      
      
      if (gpp$showDensity[pid]) {
        plf_facs <- data.frame(x = facst@exprs[,mapVariable2Channel(xvar)], y= facst@exprs[,mapVariable2Channel(yvar)])
        #match quantiles....
        qxindex <- quantile(plf$x, probs=c(0.01,0.99))
        qyindex <- quantile(plf$y, probs=c(0.01,0.99))
        qxback <- quantile(plf_facs$x, probs=c(0.01,0.99))
        qyback <- quantile(plf_facs$y, probs=c(0.01,0.99))   
        
        #plf_facs$x <- (plf_facs$x - qxback[1] + qxindex[1] ) * (qxindex[2] - qxindex[1]) / (qxback[2] - qxback[1])
        #plf_facs$y <- (plf_facs$y - qyback[1] + qyindex[1] ) * (qyindex[2] - qyindex[1]) / (qyback[2] - qyback[1])
        #make plotting faster by subsampling
        if (nrow(plf_facs) > 25000) plf_facs <- plf_facs[sample(1:nrow(plf_facs),size = 25000,replace = F),]
        out <- ggplot(data=plf_facs) + theme_classic()
        labelf_parent <- data.frame(x = min(plf_facs$x), y= max(plf_facs$y), name=parent_label)
        out <- out + stat_density2d(aes(x=x,y=y), bins=15,n =200, color = "#BBBBBB") #+ scale_fill_gradient(low="grey90",high="black",guide=F) 
        
#        out <- out + stat_density2d(aes(x=x,y=y,fill = ..level..), bins=50,geom="polygon",n =200) + scale_fill_gradient(low="grey90",high="black",guide=F) 
      } else out <- ggplot(data=plf) + theme_classic() 
      #else out <- out + geom_density2d(aes(x=x,y=y,fill=c, color=c),data = plf, bins=3, alpha = 0.5)

      out <- out + xlab(xlab) + ylab(ylab)
      out <- out + scale_x_continuous(breaks = x_breaks@exprs[,1], labels = x_labels@exprs[,1])+ scale_y_continuous(breaks = y_breaks@exprs[,1], labels = y_labels@exprs[,1])
      #out <- out + coord_cartesian(xlim=c(min(plf_facs$x),max(plf_facs$x)),  ylim  = c(min(plf_facs$y),max(plf_facs$y)) )
    displaysize <- ifelse(nrow(plf) < 250,2,1)
        if (cscale == "none") out <- out + geom_point(aes(x=x,y=y),data=plf,color="blue",size=displaysize) else if (pltype=="scCulture") out <- out + geom_point(aes(x=x,y=y,color= c),size=displaysize,data=plf) else out <- out + geom_point(aes(x=x,y=y,color= c),size=displaysize,data=plf)
      if(clab=="Srel") {
        if (pltype == "Individual1") {
          out <- out + scale_color_gradientn(colours = rev(c("#e2e2e2","black","blue","red")), limits=c(0,0.75), breaks = c(0,0.25,0.5,0.75), labels = c("0","0.25","0.5",">0.75"), oob=squish , name = clab) 
        } else {
             out <- out + scale_color_gradientn(colours = rev(c("#e2e2e2","black","blue","red")), limits=c(0,0.25), breaks = c(0,0.05,0.1,0.2), labels = c("0","0.05","0.1",">0.2"), oob=squish , name = clab) 
           }
        } else if(clab=="Srel_binned") {
          out <- out + scale_color_gradientn(colours = rev(c("#e2e2e2","black","blue","red","orange")) , name = clab) 
        } else if (cscale == "linear") {
          out <- out + scale_color_gradientn(colours = c("black","blue","red","#ffa500"), name = clab) 
          } else if (clab == "Populations" & all(unique(plf$c) %in% names(mycolors))) {
            out <- out + scale_color_manual(name = clab, values = mycolors) 
            } else  out <- out + scale_color_discrete(name = clab) #out <- out + scale_color_manual(name = clab, values = c("E.My" = "#B348FF", "E.only" = "#E80000", "Mk.My"="#00D600", "Mk.E" = "#FF7006", "Mk.E.My" = "#F492FF", "Mk.only" = "#FFDA00", "My.only" = "#0000FF"))
      if (!is.null(usedata_rect)) out <- out + geom_rect(aes(xmax=xmax,xmin=xmin,ymax=ymax,ymin=ymin), data=usedata_rect, fill=NA, color = "black") + geom_text(aes(x=xmin,y=ymin,label=name), data=usedata_rect)
      if (!is.null(usedata_poly)) out <- out + geom_polygon(aes(group=name,x=x,y=y), data=usedata_poly, fill=NA, color = "black") + geom_text(aes(x=x,y=y,label=name), data=labelf_poly)
      out <- out + geom_text(aes(x=x,y=y,label=name), hjust = 0 ,data=labelf_parent)
      out

      
      } else {
       
        out <- ggplot( data=plf) + theme_classic()
        out <- out + xlab(xlab) + ylab(ylab)
        if (exists("x_breaks")) out <- out + scale_x_continuous(breaks = x_breaks@exprs[,1], labels = x_labels@exprs[,1])
        if (exists("y_breaks")) out <- out + scale_y_continuous(breaks = y_breaks@exprs[,1], labels = y_labels@exprs[,1])
        if (cscale == "none") out <- out + geom_point(aes(x=x,y=y), color="blue")  else  out <- out + geom_point(aes(color= c, x=x, y=y),size=1)
        
        if (cscale == "linear") out <- out + scale_color_gradientn(colours = c("black","blue","red"), name = clab) else out <- out + scale_color_discrete(name = clab)
        if (!is.null(usedata_rect)) out <- out + geom_rect(aes(xmax=xmax,xmin=xmin,ymax=ymax,ymin=ymin), data=usedata_rect, fill=NA, color = "black") + geom_text(aes(x=xmin,y=ymin,label=name), data=usedata_rect)
        if (!is.null(usedata_poly)) out <- out + geom_polygon(aes(x=x,y=y, group=name), data=usedata_poly, fill=NA, color = "black") + geom_text(aes(x=x,y=y,label=name), data=labelf_poly)
        out <- out + geom_text(aes(x=x,y=y,label=name), hjust =0,data=labelf_parent)
        
         out
    }
    
  } else NULL
}


##### 2. Observers #### 
#create an observer that calls plotfunction and modifies the plots if changed.
#there are two cases in which it is triggered:
#a) if the update button is clicked
#b) if a new gate is drawn to a plot
changeplots <- observe({
  for (i in globalData$plots$id) {
    if (globalData$plots$changed[globalData$plots$id==i]) globalData$grobs[[i]] <- plotfunction(i)
    globalData$plots$changed[ globalData$plots$id ==i ] <- F
  }
})

##### 2. Reactive Elements #### 

#automatic size adjustments - only works with inline plots, but inline plots cannot be "clicked".Dumb.
width <- function(pid)
  function () { if(is.null(globalData$grobs[[pid]])) return(1) else return(500) }

height <- function(pid)
  function () {if(is.null(globalData$grobs[[pid]])) return(1) else return(400) }

#bindings to the plots, which are created by the observer above
output$Scatter1 <- renderPlot({
  globalData$grobs[[1]]
},height=height(1),width=width(1))

output$Scatter2 <- renderPlot({
  globalData$grobs[[2]]
},height=height(2),width=width(2))

output$Scatter3 <- renderPlot({
  globalData$grobs[[3]]
},height=height(3),width=width(3))

output$Scatter4 <- renderPlot({
  globalData$grobs[[4]]
},height=height(4),width=width(4))

#a possibility to save the figures (appears in the Save & Restore submodule)
output$saveFigures <- downloadHandler(filename = "indeXplorerFigures.pdf", content = function(file) {
    pdf(file,width=as.numeric(isolate(input$figWidth)),height=as.numeric(isolate(input$figHeight)))
   for (i in 1:4) print(  globalData$grobs[[i]] + theme(axis.text= element_blank(), axis.title = element_blank())) 
  dev.off()
})
