
#
repeat {
  curID <- paste(sample(chars,size=20, replace=),collapse="")
  fname <- paste("sessions/",curID, ".rda",sep="")
  if (!file.exists(fname)) break
}
plots <- isolate(globalData$plots)
gates <- isolate(globalData$Gates)
populations <- isolate(globalData$Populations)
cg <- isolate(globalData$currentGate)
genelists <- isolate(globalData$geneLists)
colorscores <- isolate(globalData$colorScores)
proj <- isolate(globalData$projections)
actds <- isolate(globalData$activeDataSet)

save(list=c("plots","gates","populations","cg", "genelists","colorscores", "proj", "actds"),file=fname)
