diffExpression <- function(population1, population2, population_df, dataset = "Individual1") {
  
  pop1 <- population_df[,population1]
  pop2 <- population_df[,population2] & !pop1

  if (all(pop1[pop2]) ) pop1 <- pop1 & !pop2 else pop2 <- pop2 & !pop1
  cat("Now loading raw data...\n")
  load(sprintf("%s/%s.rda", DATAFOLDER, dataset))

  
  
  cd <- cbind(CELLS[,pop1], CELLS[,pop2])

  sfCELLS <- estimateSizeFactorsForMatrix(CELLS)
  nCELLS <- t( t(CELLS) / sfCELLS)
  
  nCELLS <- nCELLS[rowSums(cd) > 0,]
  cd<- cd[rowSums(cd) > 0,]
  
  summary <- data.frame(pop1 = apply(nCELLS[,pop1],1,sum), pop2 = apply(nCELLS[,pop2],1,sum), primerid = rownames(cd), stringsAsFactors=F)
  
  

  coldata <- data.frame(row.names = colnames(cd),population = c(rep(population1, sum(pop1)),rep(population2, sum(pop2))))
  fdata <- data.frame(row.names=rownames(cd), key = rownames(cd))
  
    sca <- FromMatrix(as.matrix(log2(cd+1)), coldata,fdata)
  cdr2 <-colSums(assay(sca)>0)
  colData(sca)$cngeneson <- scale(cdr2)
  
  cond<-factor(colData(sca)$population)
  cond<-relevel(cond,population1)
  colData(sca)$population<-cond
  zlmCond <- zlm(~population + cngeneson, sca)

  
  summaryCond <- summary(zlmCond, doLRT=paste0("population",population2)) 
  summaryDt <- summaryCond$datatable
  fcHurdle <- merge(summaryDt[contrast==paste0("population",population2) & component=='H',.(primerid, `Pr(>Chisq)`)], #hurdle P values
                    summaryDt[contrast==paste0("population",population2) & component=='logFC', .(primerid, coef, ci.hi, ci.lo)], by='primerid') #logFC coefficients
    
  colnames(fcHurdle)[2] <- "p"
  fcHurdle$fdr <- p.adjust(fcHurdle$p, method = "fdr")
  
  rm(CELLS, SPIKES)
  return(list(result = fcHurdle, summary=summary, used_data = cd, groups = cond))
  
}

displayTooltipKharchenko <- function(data) {
  if (grepl("ENSG",data$primerid)){
    gene <- strsplit(data$primerid, "[\\(\\)]")[[1]][1]
    out <- sprintf('<b>%s</b><br>
                Click to display detailed plot', data$primerid)
  } else {
    out <- sprintf('<b>%s</b>', data$name)
  }
  return(out)
}

updateDetailPlot <- function(data, location, session) {
  if (grepl("ENSG",data$primerid)){
    updateTextInput(session, "KharchenkoDetailGene",value=data$primerid)
  }
}
