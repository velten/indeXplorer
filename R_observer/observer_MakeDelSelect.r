####### create new plot ####

if (!is.null(input$newPlot)) { #only update plots if active plot selection has not changed, otherwise give userfaces opportunity to update first
  if(input$newPlot[1] > globalData$makePlotCall) {
    globalData$makePlotCall <- input$newPlot[1] #p
    
    if (nrow(globalData$plots) < 4) {
    toadd <- data.frame(id = max(globalData$plots$id) + 1,
                        type = input$pltType, 
                        x = input$onxaxis,
                        y = input$onyaxis,
                        x_scale =input$xscale,
                        y_scale =input$yscale,
                        color_type = "none", 
                        color_variable= NA, 
                        color_scale = NA,
                        todisplay = "all",
                        tohide = "",
                        gatingMode = c("AND"),
                        changed = c(T),
                        showDensity=F,
                        stringsAsFactors = F)
    if (toadd$x == "" | is.na(toadd$x)) toadd$x <- DEFAULT_PLOT_X
    if (toadd$y == "" | is.na(toadd$y)) toadd$y <- DEFAULT_PLOT_Y
    globalData$plots <- rbind(globalData$plots, toadd)
    updateSelectInput(session, "pltNo", choices = max(globalData$plots$id))
    } else showshinyalert(session, "alert1", "Only up to four plots allowed", styleclass = "warning")
  }
}

##### delete plot ####
#shiny_bind is nasty - it cannot be deleted. Therefore "deletion" here is quite a workaround, it just makes the plot really small and removes it form the list of plots
if (!is.null(input$delPlot)) { #only update plots if active plot selection has not changed, otherwise give userfaces opportunity to update first
  if(input$delPlot[1] > globalData$deletePlotCall) {
#     #print(".")
#     globalData$deletePlotCall <- input$delPlot[1] #p
#     globalData$plots <- globalData$plots[ globalData$plots$id != globalData$activePlot,]
#     #lapply(globalData$plots$id, function(x)  bind_shiny(plots(x), paste0("test",x)))
# 
#     globalData$activePlot <- min(globalData$plots$id)
#     updateSelectInput(session, "pltNo", choices = min(globalData$plots$id))
    browser()
    
  }
}

#be smart - if a FACS marker is plotted, use biexponential scale as default


###### select plot #####
if (!is.null(input$pltNo)) { #only update plots if active plot selection has not changed, otherwise give userfaces opportunity to update first
  if (input$pltNo != globalData$activePlot) {

    globalData$activePlot = input$pltNo
    #need to immediately update plot type selector, otherwise it will change the plot type to what is *currently* selected
    updateSelectInput(session, "pltType", selected = globalData$plots$type[globalData$plots$id == globalData$activePlot])
    updateTextInput(session, "onxaxis_free", value="")
    updateTextInput(session, "onyaxis_free", value="")
    
    updateSelectInput(session, "onxaxis", selected=globalData$plots$x[globalData$plots$id == globalData$activePlot])
    updateSelectInput(session, "onyaxis", selected=globalData$plots$y[globalData$plots$id == globalData$activePlot])
    if (grepl("^FACS_",globalData$plots$x[globalData$plots$id == globalData$activePlot])) choicex <- c("linear","asinh","log","logicle") else choicex <- "linear"
    if (grepl("^FACS_",globalData$plots$y[globalData$plots$id == globalData$activePlot])) choicey <- c("linear","asinh","log","logicle") else choicey <- "linear"
    updateSelectInput(session, "xscale", selected = globalData$plots$x_scale[globalData$plots$id == globalData$activePlot],choices=choicex)
    updateSelectInput(session, "yscale", selected = globalData$plots$y_scale[globalData$plots$id == globalData$activePlot],choices=choicey)
    
    updateCheckboxInput(session, "displayContours",value= globalData$plots$showDensity[globalData$plots$id == globalData$activePlot])
    pops <- strsplit(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot], "_")[[1]]
    updateCheckboxGroupInput(session, "populations2show", selected=pops)
    hide <- strsplit(globalData$plots$tohide[globalData$plots$id == globalData$activePlot], "_")[[1]]
    updateCheckboxGroupInput(session, "populations2hide", selected=hide)
    
    updateRadioButtons(session, "gatingMode", selected = globalData$plots$gatingMode[globalData$plots$id == globalData$activePlot])
    colortype <- globalData$plots$color_type[globalData$plots$id == globalData$activePlot]
    updateSelectInput(session, "pltColorType", selected = colortype)
    
    if (colortype == "univariate") {
      updateTextInput(session, "oncaxis_free", value = "")
      updateSelectInput(session, "oncaxis", selected=globalData$plots$color_variable[globalData$plots$id == globalData$activePlot])
      updateSelectInput(session, "cscale", selected = globalData$plots$color_scale[globalData$plots$id == globalData$activePlot])
    } else if (colortype =="population") {
      sel <- strsplit(globalData$plots$color_variable[globalData$plots$id == globalData$activePlot],split="_")
      updateCheckboxGroupInput(session, "ColorPopulations", selected = sel)
    } else if (colortype == "PCA") {
      sel <- substr(globalData$plots$color_variable[globalData$plots$id == globalData$activePlot],1,1)
      updateSelectInput(session, "ColorPCA", selected = sel)
    } else if (colortype == "gene list") {
      updateSelectInput(session , "ColorGeneSet", selected =globalData$plots$color_variable[globalData$plots$id == globalData$activePlot])
      scaling <- strsplit(globalData$plots$color_scale[globalData$plots$id == globalData$activePlot], "_")[[1]][1]
      updateSelectInput(session, "ColorGeneMapping",selected = scaling)
    }
    
  } 
}

#similarily, if the dataset is changed from culture to transcriptomics or vice versa, perform update operations
if (!is.null(input$pltType)) { #only update plots if active plot selection has not changed, otherwise give userfaces opportunity to update first
  if (input$pltType != globalData$plots$type[globalData$plots$id== globalData$activePlot] ) {
    #if a marker that doesn't exist in the other dataset is currently plotted, set to default
      if ( ! input$onxaxis %in% colnames(univariates[[input$pltType]]) ) {
        updateTextInput(session, "onxaxis_free", value="")
       updateSelectInput(session, "onxaxis", selected=DEFAULT_PLOT_X,choices = DEFAULT_PLOT_X)
      }
    if ( ! input$onyaxis %in% colnames(univariates[[input$pltType]]) ) {
      updateTextInput(session, "onyaxis_free", value="")
      updateSelectInput(session, "onyaxis", selected=DEFAULT_PLOT_Y, choices= DEFAULT_PLOT_Y)
    }
    
    updateSelectInput(session, "pltColorType", selected = "none")
    updateTextInput(session, "oncaxis_free", value="")
    updateSelectInput(session, "oncaxis",selected = DEFAULT_PLOT_Y, choices=DEFAULT_PLOT_Y)
    
    updateCheckboxGroupInput(session, "populations2show", selected="all")
    updateCheckboxGroupInput(session, "populations2hide", selected="")
    
    }
}
