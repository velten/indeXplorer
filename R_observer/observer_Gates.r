getGateNames <- function(type = "Individual1") sapply(globalData$Gates[[type]], function(x) x@filterId)

getDependencies <- function(gate ) {
  #identifies gates which inherit from the given gate.
  out <- gate@filterId
  if (class(gate) == "intersectFilter") unique(unlist(c(out, sapply(gate@filters, getDependencies)))) else out
}

updateGateTree <- function(compositeGate, modifiedGate) {
  if (compositeGate@filterId == modifiedGate@filterId) modifiedGate else {
    if (class(compositeGate) == "intersectFilter") {
      out <- updateGateTree(compositeGate@filters[[1]], modifiedGate) & updateGateTree(compositeGate@filters[[2]], modifiedGate)

      out@filterId <- compositeGate@filterId
      out
      }else {
      compositeGate
    }
  }
}

setgates1<- observeEvent(input$sc1click, {
  currentGate$x_plot1 <- c(currentGate$x_plot1, input$sc1click$x)
  currentGate$y_plot1 <- c(currentGate$y_plot1, input$sc1click$y)
  print(currentGate$x_plot1 )
})

setgates2<- observeEvent(input$sc2click, {
  currentGate$x_plot2 <- c(currentGate$x_plot2, input$sc2click$x)
  currentGate$y_plot2 <- c(currentGate$y_plot2, input$sc2click$y)
})

setgates3<- observeEvent(input$sc3click, {
  currentGate$x_plot3 <- c(currentGate$x_plot3, input$sc3click$x)
  currentGate$y_plot3 <- c(currentGate$y_plot3, input$sc3click$y)
})

setgates4<- observeEvent(input$sc4click, {
  currentGate$x_plot4 <- c(currentGate$x_plot4, input$sc4click$x)
  currentGate$y_plot4 <- c(currentGate$y_plot4, input$sc4click$y)
})


#takes a gate new and computes populations.
makePopulations <- function(new, pltype) { 
  gname <- new@filterId
  
  if (!(gname %in%  getGateNames(pltype))) {
    usevars <-unname(getparams(new))
    if (any(grepl("^PC[[:digit:]]", usevars) | grepl("^GL_", usevars) )) univariates[[pltype]] <- flowFrame(as.matrix(cbind(univariates[[pltype]]@exprs, do.call(cbind,globalData$projections))))
    sub <- univariates[[pltype]][,usevars]
    f <- filter(sub, new)
    globalData$Populations[[pltype]] <- cbind(globalData$Populations[[pltype]], f@subSet) 
    #print(summary(transform(univariates[[globalData$activeDataSet]], logicleTrans)@exprs[f@subSet,usevars] ))
    colnames(globalData$Populations[[pltype]])[ncol(globalData$Populations[[pltype]])] <- gname
    globalData$Gates[[pltype]][[length(globalData$Gates[[pltype]])+1]] <-new
  } else {
    pos <- which(getGateNames(pltype) == gname)
    globalData$Gates[[pltype]][[pos]] <-new
    
    #identify gates which use said gate as parent
    for (i in 1:length(globalData$Gates[[pltype]])) globalData$Gates[[pltype]][[i]] <- updateGateTree(globalData$Gates[[pltype]][[i]],new)
    
    #identify plots which use said population as color or parent variable
    dependencies <- lapply( globalData$Gates[[pltype]], getDependencies )
    chnames <- getGateNames(pltype)[sapply(dependencies, function(x) gname %in% x)]
    
    mystrsplit <- function(x,spl) lapply(x, function(y) if(is.na(y)) return("") else unlist(strsplit(y,spl)))
    globalData$plots$changed[sapply(mystrsplit(globalData$plots$todisplay, "_"), function(x) any(chnames %in% x)) | 
                               (sapply(mystrsplit(globalData$plots$color_variable, "_"), function(x) any(chnames %in% x)) & globalData$plots$color_type == "population")] <- T
    
    
    for (g in 1:length(globalData$Gates[[pltype]])) {
      current <- globalData$Gates[[pltype]][[g]]
      usevars <-unname(getparams(current))
      sub <- univariates[[pltype]][,usevars]
      f <- filter(sub, current)
      globalData$Populations[[pltype]][,getGateNames(pltype)[g]] <- f@subSet
    }
    
    
  } 
}

makeGate <- function(pid, xcoords, ycoords) {

  xvar <- globalData$plots$x[globalData$plots$id == pid]
  yvar <- globalData$plots$y[globalData$plots$id == pid]
  pltype <- globalData$plots$type[globalData$plots$id == pid]
  
  #what transform was used for the plot?
  
  xt <- switch(globalData$plots$x_scale[globalData$plots$id == pid],
               logicle = logicleTrans[[pltype]]@transforms[[mapVariable2Channel(xvar)]]@f,
               log = function(x) ifelse(x > 0, log(x), 0),
               linear = function(x) x,
               asinh = asinh
  )
  yt <- switch(globalData$plots$y_scale[globalData$plots$id == pid],
               logicle = logicleTrans[[pltype]]@transforms[[mapVariable2Channel(yvar)]]@f,
               log = function(x) ifelse(x > 0, log(x), 0),
               linear = function(x) x,
               asinh = asinh
  )
  trans <- transformList(from = c(mapVariable2Channel(xvar),mapVariable2Channel(yvar)),
                         tfun = list(xt,yt),
                         transformationId = "facstrans")
  if (length(xcoords) > 1) {
    if (length(xcoords) == 2) {
      xcoords <- xcoords[order(xcoords)]; ycoords <- ycoords[order(ycoords)];
      
      gatem <- matrix( c(xcoords, ycoords) ,byrow=F, nrow=2)
      colnames(gatem) <- c(mapVariable2Channel( xvar),mapVariable2Channel(yvar ))
      gname <- ifelse(is.null(input$gateName),"NewGate",input$gateName)
      new <- rectangleGate(.gate = gatem) %on% trans
      new@filterId <- gname
    } else if (length(xcoords) > 2) {
      gatem <- matrix( c(xcoords, ycoords) ,byrow=F, ncol=2)
      colnames(gatem) <- c(mapVariable2Channel( xvar),mapVariable2Channel(yvar ))
      gname <- ifelse(is.null(input$gateName),"NewGate",input$gateName)
      new <- polygonGate(.gate = gatem) %on% trans
      #combine gates with parent gates
    }
    
    usegates <- strsplit(globalData$plots$todisplay[globalData$plots$id == pid],split="_")[[1]]
    if (usegates[1] != "all"){
      out <- globalData$Gates[[pltype]][[ which(getGateNames(pltype) == usegates[1]) ]]
      if (length(usegates) >= 2){
        for (i in 2:length(usegates)) {
          if (globalData$plots$gatingMode[globalData$plots$id == pid] == "AND") out <- out & globalData$Gates[[pltype]][[ which(getGateNames(pltype) == usegates[i])]]
          if (globalData$plots$gatingMode[globalData$plots$id == pid] == "OR") out <- out | globalData$Gates[[pltype]][[ which(getGateNames(pltype) == usegates[i])]]
        }}
      new <- out & new
      
    }
    
    #if(gname %in% getGateNames())  else pos <- length( globalData$Gates)+1
    new@filterId <- gname
    
    if (exists("new")) {
      makePopulations(new, pltype)
      
    }
    globalData$plots$changed[globalData$plots$id == pid] <- T
  }
}

creategates1 <- observeEvent(input$sc1dbl, {
  
 xp <- c(currentGate$x_plot1, input$sc1dbl$x)
 yp <- c(currentGate$y_plot1, input$sc1dbl$y)
 #3.483006
 #browser()
  makeGate(1, xp, yp)
 currentGate$x_plot1 <- c()
 currentGate$y_plot1 <- c()
})


creategates2 <- observeEvent(input$sc2dbl, {
  
  xp <- c(currentGate$x_plot2, input$sc2dbl$x)
  yp <- c(currentGate$y_plot2, input$sc2dbl$y)
  makeGate(2, xp, yp)
  currentGate$x_plot2 <- c()
  currentGate$y_plot2 <- c()
})

creategates3 <- observeEvent(input$sc3dbl, {
  
  xp <- c(currentGate$x_plot3, input$sc3dbl$x)
  yp <- c(currentGate$y_plot3, input$sc3dbl$y)
  makeGate(3, xp, yp)
  currentGate$x_plot3 <- c()
  currentGate$y_plot3 <- c()
})

creategates4 <- observeEvent(input$sc4dbl, {
  
  xp <- c(currentGate$x_plot4, input$sc4dbl$x)
  yp <- c(currentGate$y_plot4, input$sc4dbl$y)
  makeGate(4, xp, yp)
  currentGate$x_plot4 <- c()
  currentGate$y_plot4 <- c()
})
