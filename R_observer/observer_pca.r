

# the following obsever is triggered when the button is pressed
runPCA <- observe({
  
  #function to perform PCA, store results in the global reactiveValues object pca
  doit <- function() {
    #select genes, cells to perform the analysis on
    if (input$PCAGeneList == "All genes") usegenes <- allgenes[[globalData$activeDataSet]] else usegenes <- globalData$geneLists[[input$PCAGeneList]]
    
    usepopulations <- input$PCAPopulation
    if(length(usepopulations) == 1) usecells <- globalData$Populations[[globalData$activeDataSet]][,usepopulations] else {
      usecells <- apply(globalData$Populations[[globalData$activeDataSet]][,usepopulations],1,max)==1
    }
    pca$usecells <- usecells
    
    
    pcadata <- univariates[[globalData$activeDataSet]]@exprs[usecells, usegenes]
    showshinyalert(session,"pcaalert",HTMLtext="Computing PCA...",styleclass="info")
    
    result <- prcomp(pcadata, scale.=F)
    
    signs <- apply(result$rotation, 2, function(x) sign(sum(x))) 
    
    pca$scores <- t(t(result$x) * signs)
    
    pca$loadings <- t(t(result$rotation) * signs)
    pca$variance_observed <- result$sdev^2# / sum(result$sdev^2)
    
    #compute background variance
    showshinyalert(session,"pcaalert",HTMLtext="Performing Bootstrap computation...",styleclass="info")
    #scrambled_sdev <- replicate(n=2, {
    scrambled <- t(apply(univariates[[globalData$activeDataSet]]@exprs[usecells, usegenes] , 1 , function(x ) sample(x, length(usegenes),replace=F)))
    sdevs <- prcomp(scrambled, scale.=F)$sdev^2
    #sdevs# / sum(sdevs)
    #})
    
    #pca$variance_simulated <- apply(scrambled_sdev,1,mean)
    pca$variance_simulated <- sdevs
    
    showshinyalert(session,"pcaalert",HTMLtext="Computations successfully completed...",styleclass="success")
    
    
    if (!pca$binding) {
      bind_shiny(scoresplot, plot_id="ScoresPlot")
      bind_shiny(loadingsplot, plot_id = "LoadingsPlot")
      bind_shiny(varianceplot, plot_id = "VariancePlot")
    }
    pca$binding <- T
    
  }
  
  #if PCA button is clicked ,perform PCA and store results
  if (!is.null(input$PCARun)) {
    if (pca$runCall != input$PCARun[1]){
      pca$runCall <- input$PCARun[1]
      doit()
    }
  }

  
})

##### gene list storage ####
#observer to monitor whether the uses clicks the "store gene list" button in the loadings table panel
storePCAGL <- observe({
  if (!is.null(input$PCAStoreGL)) {
    if (pca$storeCall != input$PCAStoreGL[1]){
      pca$storeCall <- input$PCAStoreGL[1]
      if (nrow(pca$loadings) > 0) {
        sorted <- pca$loadings[order(pca$loadings[,as.integer(input$PCAGeneListPC)], decreasing = input$PCAGeneListTop == "Top"),]
        
        use <- rownames(sorted)[1:input$PCAGeneListNumber]
        name <- paste("PCALoadings.PC", input$PCAGeneListPC, input$PCAGeneListTop, sep=".")
        globalData$geneLists[[name]] <- use
     
    }
    }}
})

##### creation of the scores plot ####
scoresplot <- reactive({
  usecells <- pca$usecells
  usepop <- input$PCAHighlightPopulation[which(input$PCAHighlightPopulation %in% colnames(globalData$Populations[[globalData$activeDataSet]]))]
  if (length(usepop) == 1) pca$labels <- ifelse(globalData$Populations[[globalData$activeDataSet]][usecells,usepop],input$PCAHighlightPopulation, "other")
  else pca$labels <- and_connect(globalData$Populations[[globalData$activeDataSet]][usecells,usepop])
  
  plf <- data.frame(x = pca$scores[, as.integer(input$PCAx)], y = pca$scores[, as.integer(input$PCAy)], label = pca$labels)
  out <- plf %>% ggvis(~ x, ~ y, fill = ~ label) %>% layer_points() %>% 
    add_axis("x", title = paste("PC", input$PCAx)) %>% add_axis("y", title=paste("PC", input$PCAy)) %>% add_legend("fill", title = "Population") %>%
    set_options(width = 400, height=350)
  return(out)
})

##### creation of the loadings plot ####

loadingsplot <- reactive({
  
  if (input$PCAHighlightGeneList == "None") highlight <- "None" else highlight <- ifelse(rownames(pca$loadings) %in% globalData$geneLists[[input$PCAHighlightGeneList]], input$PCAHighlightGeneList, "other")
  
  
  
  plf <- data.frame(x = pca$loadings[, as.integer(input$PCAx)], y = pca$loadings[, as.integer(input$PCAy)], label = sapply(strsplit(rownames(pca$loadings), " \\("), function(x) x[1]), highlight=highlight, name = rownames(pca$loadings))
  textf <- subset(plf, rank(x) < 25 | rank(x) > length(x)-25 | rank(y) < 25 | rank(y) > length(y)-25)

  out <- plf %>% ggvis(~ x, ~ y, key := ~name) %>% layer_points(size := 2, fill = ~ highlight) %>% layer_text(fontSize:=8 , text :=~ label,fill = ~ highlight,data=textf) %>%
    add_axis("x", title = paste("PC", input$PCAx)) %>% add_axis("y", title=paste("PC", input$PCAy)) %>% add_legend("fill", title = "Gene List") %>%
    set_options(width = 400, height=350) %>% add_tooltip(displayTooltipPCA,on="hover") %>% handle_click(openGenecards)
  
  if (input$PCAHighlightGeneList != "None") {
    pvalx <- sprintf("px = %1.2e", wilcox.test(plf$x[plf$name %in% globalData$geneLists[[input$PCAHighlightGeneList]] ], plf$x[!(plf$name %in% globalData$geneLists[[input$PCAHighlightGeneList]] )])$p.value )
    pvaly <- sprintf( "py = %1.2e", wilcox.test(plf$y[plf$name %in% globalData$geneLists[[input$PCAHighlightGeneList]] ], plf$y[!(plf$name %in% globalData$geneLists[[input$PCAHighlightGeneList]] )])$p.value)
    pvalf <- data.frame(x = c(max(plf$x), max(plf$x)), y = c(max(plf$y), 0.9*max(plf$y)), name = c("pvalx","pvaly"), label = c(pvalx, pvaly))
    out <- layer_text(out, text := ~label, fill := "black", baseline := "bottom", align := "right",dx := 0, dy := 0, stroke := "black", data=pvalf)
  }
  
  
  return(out)
})


displayTooltipPCA <- function(data) {

  if (data$name %in% c("pvalx","pvaly")) out <- "p-value from Wilcoxon Ranksum test" else out <- sprintf('<b>%s</b><br>Click to open genecards', data$name)
  return(out)
}

varianceplot <- reactive({
  ncomp <- min(c(length(pca$variance_observed),30))
  plf <- data.frame(pc = rep(1:ncomp,2), variance = c(pca$variance_observed[1:ncomp], pca$variance_simulated[1:ncomp]), label = rep(c("Observed","Bootstrap"), each =ncomp))
  out <- plf %>% ggvis( ~ pc, ~ variance, fill = ~label) %>% layer_points() %>% 
    add_axis("x", title = "Principle Component") %>% add_axis("y", title="% of variance explained") %>% add_legend("fill", title = "") %>%
    set_options(width = 400, height=350)
  out
})

#the plot which checks for correlation of PCs with FACS markers
output$FACScorplot <- renderPlot({
   facs <- univariates[[globalData$activeDataSet]]@exprs[pca$usecells,grepl("^FACS_",colnames(univariates[[globalData$activeDataSet]]@exprs))]
   facs <- apply(facs,2,oldtransform, how="biexponential")
  colnames(facs) <- substr(colnames(facs), 6,100)
  
  pcr <- apply(facs, 2, function(x) lm(x ~ pca$scores[,1:8]))
  summaries <- lapply(pcr, function(l) summary(l))
  rsqr <- sapply(summaries, function(s) s$adj.r.squared)
  pca$totalRsqr <- sum(rsqr)
   
   pval <- apply(facs,2,function(f) {
     apply(pca$scores[,1:8],2,function(y,x){
       cor.test(x,y)$p.value
     },x=f)
   })
   require(ggplot2)
   require(reshape2)
   plf <- melt(pval)
   colnames(plf) <- c("PC","Marker","p.value")
   #plf$PC <- as.integer(substr(plf$PC, 17,100))
   plf$p.value <- p.adjust(plf$p.value,method="fdr")
   plf$p.value[plf$p.value == 0] <- 1e-16
   plf$p.value <- log10(plf$p.value)
 
  
  rsqr[rsqr<0 ] <- 0
  association <- qplot(y = Marker, x = PC, fill = p.value, geom="tile", data=plf) + scale_fill_gradient(low="red",high="blue",na.value="white", name = expression(log[10](p))) + ylab("") + theme_minimal() + xlab("") + theme(legend.position="top", axis.ticks.y=element_blank(), panel.border = element_rect(fill=NA, colour="black"), panel.grid=element_blank(), axis.text.x = element_text(angle=90,hjust = 1,size=12)) + scale_y_discrete(breaks=names(rsqr))
  
  rsqr_plot <- qplot(x = "1.0", y = names(rsqr), fill = rsqr, geom="tile") + scale_fill_gradient(low="white",high="red",name=expression(R^2))  + xlab("") + theme_minimal() + theme(axis.ticks.x = element_blank(), legend.position="top",panel.border = element_rect(fill=NA, colour="black"), panel.grid=element_blank(), axis.text.x = element_text(angle=90,vjust = 1,size=12,colour="white")) + ylab("")+ scale_y_discrete(breaks=names(rsqr))
  
  require(grid)
  vplayout <- function(x,y) viewport(layout.pos.row=x,layout.pos.col=y)
  grid.newpage()
  pushViewport(viewport(layout = grid.layout(1,18)))
  print(rsqr_plot, vp = vplayout(1, 1:4))
  
  print(association, vp = vplayout(1, 5:18))

  
},width=800,height=600)

#the links to GOrilla in the Gene Ontology panel
output$PCAGO <- renderUI({
  
  if (nrow(pca$loadings) < 1000) return(tags$p("GO einrichment analysis will only be performed if a PCA has been performed on more than 1000 genes")) else {
  
    #only run for principle components that exceed the random noise
    torun <- input$PCAGOnumber
    
  geneIDs <- sapply(strsplit(rownames(pca$loadings), "[\\(\\)]"), function(x) x[2])
  ranked <- apply(pca$loadings,2,rank)
  urls <- lapply(1:torun, function(i) {
  
  #inUniverse <- df$MeanExpression >= min(df$MeanExpression[df$fdr < input$BrenneckeFDRThresh])
  inUniverse <- rep(T, length(geneIDs))
  
  #genes with biggest 10% of loadings
  inSelection <- ranked[,i] > 0.98 * length(ranked[,i])
  # id <- paste(sample(chars,6),collapse=""); hitfile <- paste("tmp/",id,"_hits.txt",sep=""); restfile <- paste("tmp/",id,"_rest.txt",sep="")
  #writeLines( geneIDs[inSelection], con= hitfile)
  #writeLines( geneIDs[inUniverse & !inSelection], con= restfile)
  #command <- paste(GORILLACOMMAND, hitfile, restfile, "proc", SPECIES)
  #url_bigloadings <- system(command, intern=T)
  url_bigloadings <- getGOrilla(geneIDs[inSelection], geneIDs[inUniverse & !inSelection], "proc", SPECIES)
  
  #genes with smallest 10% of loadings
  inSelection <- ranked[,i] < 0.02 * length(ranked[,i])
  # id <- paste(sample(chars,6),collapse=""); hitfile <- paste("tmp/",id,"_hits.txt",sep=""); restfile <- paste("tmp/",id,"_rest.txt",sep="")
  # writeLines( geneIDs[inSelection], con= hitfile)
  # writeLines( geneIDs[inUniverse & !inSelection], con= restfile)
  # command <- paste(GORILLACOMMAND, hitfile, restfile, "proc", SPECIES)
  # url_smallloadings <- system(command, intern=T)
  url_smallloadings <- getGOrilla(geneIDs[inSelection], geneIDs[inUniverse & !inSelection], "proc", SPECIES)
  
  
  out <- tags$li(
    paste("PC ",i,":",sep=""),
    tags$a(href=url_bigloadings,target="_blank","Top 10% of loadings"),
    tags$a(href=url_smallloadings,target="_blank","Bottom 10% of loadings")
    )
  out
  })
  return(tags$ul(urls))
  }
})
