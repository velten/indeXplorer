#this observer checks whether the KharchenkoRun action button is clicked, and if so, 
#performs the required analysis
#in this module, computations are send to background by using mcparallel so that the user can continue
#with further analyses. A javascript call to setInterval function (in tutorial.js) triggers mccollect 
#through the observer  checkresult. if the user closes the session while the computations are still 
#active, they are not cancelled (as is the nature of mccollect) but rather finish, and the user is 
#notified via e-mail.
require(sendmailR)
performKharchenkoAnalysis <- observe({
  if (!is.null(input$KharchenkoRun)) {
    if (kharchenko$runCall != input$KharchenkoRun[1]){
      kharchenko$runCall <- input$KharchenkoRun[1]
        #run the analysis
      #browser()
      #if the same populations have been tested before, just oad results.
      population_df <- isolate(globalData$Populations[[globalData$activeDataSet]])
      
      kharchenko$pop1 <- input$KharchenkoPopulation1
      kharchenko$pop2 <- input$KharchenkoPopulation2
      
      result <- diffExpression(input$KharchenkoPopulation1, input$KharchenkoPopulation2, population_df=population_df, dataset = globalData$activeDataSet)  
     
        #store the result in the kharchenko reactiveValues instance
        #result = ediff, used_data = cd, models = o.ifm, prior = o.prior, groups = sg, summary = summary))
       kharchenko$used_data = result$used_data
      kharchenko$summary = result$summary
      kharchenko$result = result$result
      # kharchenko$prior = result$prior
      kharchenko$groups = result$groups
      # kharchenko$models = result$models
              if (!kharchenko$binding) bind_shiny(kharchenkoReactive, plot_id="KharchenkoPlot")
        kharchenko$binding <- T
      }
    }
})


#observer to create a geneList from the differential expression result
storeKharchenkoGL <- observe({
  if (!is.null(input$KharchenkoStoreGL)) {
    if (kharchenko$storeCall != input$KharchenkoStoreGL[1]){
      kharchenko$storeCall <- input$KharchenkoStoreGL[1]
      if (nrow(kharchenko$result) > 0) {
        # if (kharchenko$pop1 > kharchenko$pop2) {
              regu_use <- switch(input$KharchenkoGenes2Store,
                     All = kharchenko$result$fdr < input$KharchenkoFCThresh,
                     Upregulated =  kharchenko$result$fdr < input$KharchenkoFCThresh &  kharchenko$result$coef < 0,
                     Downregulated = kharchenko$result$fdr < input$KharchenkoFCThresh &  kharchenko$result$coef > 0
              )
                      #) } else {
                     #   regu_use <- switch(input$KharchenkoGenes2Store,
                     #                      All = kharchenko$result$ce != 0,
                     #                      Upregulated =  kharchenko$result$ce > 0,
                     #                      Downregulated = kharchenko$result$ce < 0
                     #   )   
                     #       }
        
         use <- na.omit(kharchenko$result$primerid[ regu_use])
        name <- paste("DiffExpression", paste(input$KharchenkoPopulation1, input$KharchenkoPopulation2,sep="_"), input$KharchenkoGenes2Store, sep=".")
        globalData$geneLists[[name]] <- use
      } else {
        showshinyalert(session,"KharchenkGOalert",HTMLtext="You need to Run the differential expression analysis first",styleclass="warning")
      }
    }}
})

#observer to pass data to GOrilla if the GO analysis button is clicked 
performKharchenkoGO <- observe({
  
  if (!is.null(input$KharchenkoGO)) {
    if (kharchenko$GOCall != input$KharchenkoGO[1]) {
      kharchenko$GOCall <- input$KharchenkoGO[1]
      df <- merge(isolate(kharchenko$result), isolate(kharchenko$summary))
      df <- na.omit(df)
      if (nrow(df) > 0) {
        showshinyalert(session,"KharchenkoGOalert",HTMLtext="Performing GO analysis...",styleclass="info")
        geneIDs <- sapply(strsplit(df$primerid, "[\\(\\)]"), function(x) x[2])
        
        totalexp <- df$pop1 + df$pop2
        minx <- min( totalexp[df$fdr < input$KharchenkoFCThresh] )
        inUniverse <- totalexp >= minx
        #inUniverse <- rep(T, length(geneIDs))
        
        # if (kharchenko$pop1 > kharchenko$pop2) {
        regu_use <- switch(input$KharchenkoGenes2Store,
                           All = kharchenko$result$fdr < input$KharchenkoFCThresh,
                           Upregulated =  kharchenko$result$fdr < input$KharchenkoFCThresh &  kharchenko$result$coef < 0,
                           Downregulated = kharchenko$result$fdr < input$KharchenkoFCThresh &  kharchenko$result$coef > 0
        )
          # ) } else {
          #   regu_use <- switch(input$KharchenkoGenes2Store,
          #                      All = kharchenko$result$ce != 0,
          #                      Upregulated =  kharchenko$result$ce > 0,
          #                      Downregulated = kharchenko$result$ce < 0
          #   )   
          # }
     inSelection <- regu_use
     #goResult <- topGOAnalysis(geneIDs, inUniverse, inSelection)
     #kharchenko$go <- do.call(rbind, goResult)
        # id <- paste(sample(chars,6),collapse=""); hitfile <- paste("tmp/",id,"_hits.txt",sep=""); restfile <- paste("tmp/",id,"_rest.txt",sep="")
        # writeLines( geneIDs[inSelection], con= hitfile)
        # writeLines( geneIDs[inUniverse & !inSelection], con= restfile)
        # 
        # command <- paste(GORILLACOMMAND, hitfile, restfile, "proc",SPECIES)
        # url <- system(command, intern=T)
        # print(url)
     url <- getGOrilla(geneIDs[inSelection], geneIDs[inUniverse & !inSelection], "proc", SPECIES)
     
        showshinyalert(session,"KharchenkoGOalert",HTMLtext=paste("GO results are available at <a href='",url, "', target='_blank'>GOrilla</a>",sep=""),styleclass="success")
        #updateSelectInput(session, "KharchenkoTableToDisplay", selected="Gene Ontology")
      } else {
        showshinyalert(session,"KharchenkoGOalert",HTMLtext="You need to Run the gene variability analysis first",styleclass="warning")
      }
    }}
})

#creates the plot. has direct dependencies on gene list selection and FDR threshold so that
#the plot is redrawn immediately if these inputs change.
kharchenkoReactive <- reactive({
  if(nrow(kharchenko$summary) == 0) return() else{
    plotf <- merge(isolate(kharchenko$summary), isolate(kharchenko$result))
    #print(head(plotf))
    #set up plotting data: choose points to highlight
    if (input$KharchenkoGL == "none" | input$KharchenkoGL == "") plotf$highlight = "other" else plotf$highlight <- ifelse(plotf$primerid %in% globalData$geneLists[[input$KharchenkoGL]], input$KharchenkoGL, "other" )
    #plotf$sig <- ifelse(plotf$ce != 0, "ce != 0", "n.s.")
    plotf$sig <- ifelse(plotf$fdr < input$KharchenkoFCThresh, "significant", "n.s.")
    
    #transform to log10 because ggvis scale transform sucks
    plotf$pop1 <- log10(plotf$pop1+1); plotf$pop2 <- log10(plotf$pop2+1)
    plotf <- na.omit(plotf)
    
    #http://rpackages.ianhowson.com/cran/ggvis/man/add_legend.html
    out <- plotf %>% ggvis(x =~ pop1, y=~ pop2, fill =~ sig, key := ~primerid) %>%
      layer_points(size =~ highlight, shape =~ highlight) %>% add_axis("x", title =  kharchenko$pop1) %>% add_axis("y", title = kharchenko$pop2) %>%
      #layer_points(size =~ highlight, shape =~ highlight) %>% add_axis("x", title = pop1) %>% add_axis("y", title = pop2) %>%
      scale_nominal("size", domain=c("other",input$KharchenkoGL),range=c(ifelse(input$KharchenkoGL == "none", 12,3),25)) %>%  scale_nominal("shape", domain=c("other",input$KharchenkoGL),range=c("circle","triangle-up")) %>%
      add_legend("fill", title = "Significance") %>% add_legend(c("shape", "size", "fillOpacity"), title = "Gene List", properties=legend_props(legend=list(y = 50))) %>%
      add_tooltip(displayTooltipKharchenko, "hover") %>%
      set_options(width=450,height=400)

    #determine p-value for enrichment, add to plot
    if (input$KharchenkoGL != "none" & input$KharchenkoGL != ""){
      #throw out genes of low expression for which no statement can be made
      totalexp <- kharchenko$summary$pop1 + kharchenko$summary$pop2
      minx <- min( totalexp[plotf$sig != "n.s."] )
    `sig&gl` <- sum(plotf$sig == "significant" & plotf$highlight != "other")
    `notsig&gl` <- sum(plotf$sig != "significant" & plotf$highlight != "other" & totalexp >= minx)
    `sig&notgl` <- sum(plotf$sig == "significant" & plotf$highlight == "other")
    `notsig&notgl` <- sum(plotf$sig != "significant" & plotf$highlight == "other" & totalexp >= minx)
    testable <- matrix(data=c(`sig&gl` , `notsig&gl` , `sig&notgl` , `notsig&notgl`),nrow=2,byrow=T)
    pval_gl_enrichment <- fisher.test(testable,alternative="greater")$p.value
    textf <- data.frame(pop1 = max(plotf$pop1), pop2 = max(plotf$pop2), name = "significance for enrichment", text = sprintf("p = %1.2e",pval_gl_enrichment), primerid = plotf$primerid[1])
    out <- layer_text(out, text := ~text, fill := "black", baseline := "bottom", align := "right",dx := 0, dy := 0, stroke := "black", data=textf)
    }
    return(out)
  }
})

#this plot is drawn if the user clicks on a gene, or types a gene name in the "select gene to view in detail" box
output$KharchenkoDetailPlot<- renderPlot({
  NULL
  })
