#use an update button - do not listen to individual clicks

if (!is.null(input$update)) {
  #a function to perform PCA on a gene list - to plot the result on an axis or use as a colorscale
  pcaplot <- function(genelist=NULL,outname, whichpc = 1, pltype = "Individual1") { 
            usepopulations <-  strsplit(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot], split="_")[[1]]
            if(length(usepopulations) == 1) usecells <- globalData$Populations[[pltype]][,usepopulations] else{
              usecells <- apply(globalData$Populations[[pltype]][,usepopulations],1,max)==1
            }
            
          if (is.null(genelist)) usegenes <- allgenes[[globalData$activeDataSet]] else usegenes <- globalData$geneLists[[genelist]]
            
          
              pcadata <- univariates[[globalData$activeDataSet]]@exprs[usecells, usegenes]
            
            
            pca <- prcomp(pcadata, scale.=F)
            #out <- rep(NA, nrow(pca$x))
            out <- rep(NA, nrow(univariates[[globalData$activeDataSet]]))
            out[usecells]<- sign(sum(pca$rotation[,whichpc])) * pca$x[,whichpc]
            out
}

  if(input$update[1] > globalData$updatePlotCall) {

    globalData$updatePlotCall <- input$update[1]
    globalData$plots$type[globalData$plots$id == globalData$activePlot] <-input$pltType

        if(!is.null(input$populations2show)){
      globalData$plots$todisplay[globalData$plots$id == globalData$activePlot] <- paste(input$populations2show,collapse="_")
        }
    
    if(!is.null(input$populations2hide)){
      globalData$plots$tohide[globalData$plots$id == globalData$activePlot] <- paste(input$populations2hide,collapse="_")
    } else globalData$plots$tohide[globalData$plots$id == globalData$activePlot] <- ""
    
    if (!is.null(input$displayGates)) {
      globalData$plots$showgates[globalData$plots$id == globalData$activePlot] <- input$displayGates
    }
    
    #onxaxis
    if(!is.null(input$onxaxis) & input$onxaxis != "") {

      globalData$plots$x[globalData$plots$id == globalData$activePlot] <- input$onxaxis
      if(substr(input$onxaxis,1,3) == "GL_") {
        label <- paste(input$onxaxis, paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep=".")
        globalData$plots$x[globalData$plots$id == globalData$activePlot] <- label
        gl <- substr(input$onxaxis,4,500)
        pc <- as.integer(substr(gl, nchar(gl)-1,nchar(gl)))
        globalData$projections[[label]] <- pcaplot(substr(gl, 1,nchar(gl)-5), input$onxaxis, pc, pltype = globalData$activeDataSet)
      }
      if (nchar(input$onxaxis) == 3 & substr(input$onxaxis,1,2) == "PC") {
        label <- paste(input$onxaxis, paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep=".")
        globalData$plots$x[globalData$plots$id == globalData$activePlot] <- label
        pc <- as.integer(substr(input$onxaxis, 3,3))
        globalData$projections[[label]] <- pcaplot(NULL, input$onxaxis, pc, pltype = globalData$activeDataSet)
      }
    }
    
    #onyaxis
    if(!is.null(input$onyaxis)& input$onyaxis != "") {
      globalData$plots$y[globalData$plots$id == globalData$activePlot] <- input$onyaxis
      if(substr(input$onyaxis,1,3) == "GL_") {
        label <- paste(input$onyaxis, paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep=".")
        globalData$plots$y[globalData$plots$id == globalData$activePlot] <- label
        gl <- substr(input$onyaxis,4,500)
        pc <- as.integer(substr(gl, nchar(gl)-1,nchar(gl)))
        globalData$projections[[label]] <- pcaplot(substr(gl, 1,nchar(gl)-5), input$onyaxis, pc, pltype = globalData$activeDataSet)
      }
      if (nchar(input$onyaxis) == 3 & substr(input$onyaxis,1,2) == "PC") {
        label <- paste(input$onyaxis, paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep=".")
        globalData$plots$y[globalData$plots$id == globalData$activePlot] <- label
        pc <- as.integer(substr(input$onyaxis, 3,3))
        globalData$projections[[label]] <- pcaplot(NULL, input$onyaxis, pc, pltype = globalData$activeDataSet)
      }
    }
    #xscale
    if(!is.null(input$xscale)) {
      globalData$plots$x_scale[globalData$plots$id == globalData$activePlot] <- input$xscale
    }
    #yscale
    if(!is.null(input$yscale)) {
      globalData$plots$y_scale[globalData$plots$id == globalData$activePlot] <- input$yscale
    }
    
    #color type 
    if (!is.null(input$pltColorType)) {
    globalData$plots$color_type[globalData$plots$id == globalData$activePlot] <- input$pltColorType
    
    
    #color
    if (!is.null(input$oncaxis)) {
      if (input$oncaxis != "" & input$pltColorType == "univariate"){
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- input$oncaxis
      }
    }
    
    #color transform
    if (!is.null(input$cscale)& input$pltColorType == "univariate") {
      globalData$plots$color_scale[globalData$plots$id == globalData$activePlot] <- input$cscale
    }
    
    #color populations
    if (!is.null(input$ColorPopulations) & input$pltColorType == "population") {
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- paste(input$ColorPopulations, collapse = "_")
      
    }
    
    if (!is.null(input$ColorClustSelect) & input$pltColorType == "clustering") {
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- input$ColorClustSelect
    }
    

    if(!is.null(input$ColorGeneSet) & input$pltColorType == "gene list") {
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- input$ColorGeneSet #store the name of the gene set as the variable
      globalData$plots$color_scale[globalData$plots$id == globalData$activePlot] <- paste(input$ColorGeneMapping,paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep="_")
      #then, compute the mapping and store. Purpose: so that it needs not be recomputed when the plots are redrawn.
      colScoreID <- removeSpaces(paste(input$ColorGeneSet, input$ColorGeneMapping,paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep="_"))
      
      if (input$ColorGeneMapping == "PCA projection") {
        globalData$colorScores[[colScoreID]] <- pcaplot(input$ColorGeneSet,colScoreID, pltype = globalData$activeDataSet)
      } else if (input$ColorGeneMapping == "Sum of raw expression values") {
        load(sprintf("%s/%s.rda", DATAFOLDER, globalData$activeDataSet))
        globalData$colorScores[[colScoreID]] <- log10(apply(CELLS[globalData$geneLists[[input$ColorGeneSet]],], 2,sum)+1)
        rm(CELLS, SPIKES)
      }
    
    }
    
    if (!is.null(input$ColorPCA) & input$pltColorType== "PCA") {
      
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- paste(input$ColorPCA,paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep="")
      globalData$plots$color_scale[globalData$plots$id == globalData$activePlot] <- "PC"
      #then, compute the mapping and store. Purpose: so that it needs not be recomputed when the plots are redrawn.
      colScoreID <- paste("PC", input$ColorPCA,paste(globalData$plots$todisplay[globalData$plots$id == globalData$activePlot],collapse="_"),sep="")
      globalData$colorScores[[colScoreID]] <- pcaplot(NULL,colScoreID,as.integer(input$ColorPCA), pltype = globalData$activeDataSet)
      
      
    }
    
    
    
    
    } else {
      globalData$plots$color_type[globalData$plots$id == globalData$activePlot] <- "none"
      globalData$plots$color_variable[globalData$plots$id == globalData$activePlot] <- NA
      globalData$plots$color_scale[globalData$plots$id == globalData$activePlot] <- NA
    }
    

    if (!is.null(input$displayContours)) {
      globalData$plots$showDensity[globalData$plots$id == globalData$activePlot] <- input$displayContours
    }
    
    if (!is.null(input$gatingMode)) {
      globalData$plots$gatingMode[globalData$plots$id == globalData$activePlot] <- input$gatingMode
    }
   
    globalData$plots$changed[globalData$plots$id == globalData$activePlot] <- T
    
  }

  
}
