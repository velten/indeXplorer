createGL <- observe({
  #when the create gene list button is clicked
  if (!is.null(input$createGeneList)) {
    if (globalData$GLCreateCall != input$createGeneList[1]){
      globalData$GLCreateCall <- input$createGeneList[1]
      if (input$geneListName =="") {
        showshinyalert(session,"GLalert",HTMLtext="No name supplied",styleclass="warning")
        return()
      } else if (input$geneListName %in% names(globalData$geneLists)) {
        showshinyalert(session,"GLalert",HTMLtext=paste("The gene list",input$geneListName,"will be overwritten. Please wait while gene list is loading."),styleclass="warning")
      } else {
        showshinyalert(session,"GLalert",HTMLtext=paste("Please wait while gene list is loading."),styleclass="info")
      }
      
  if (input$geneListType == "File Upload") {
    if (is.null(input$uploadGeneList)) {
      showshinyalert(session,"GLalert",HTMLtext="No file selected",styleclass="warning")
      return()
    } else {
      genes <- readLines(input$uploadGeneList$datapath)
      converted <- unlist(sapply(genes, checkInput,martf=martf[paste0(martf[,2], " (", martf$ensembl_gene_id,")") %in% allgenes[[globalData$activeDataSet]], ], uv = univariates[[globalData$activeDataSet]] ))
      if (is.null(converted)) {
        showshinyalert(session,"GLalert",HTMLtext="File contained no valid gene identifiers",styleclass="warning")
        return()
      } else {
      globalData$geneLists[[input$geneListName]] <- converted
      showshinyalert(session,"GLalert",HTMLtext="Gene list is now available",styleclass="success")
      
      }     
    }
  } else if (input$geneListType == "Set operation") {
    if (is.null(input$GLSetMembers)) showshinyalert(session,"GLalert",HTMLtext=paste("No gene sets selected to apply set operation to"),styleclass="warning") else {
      res <- globalData$geneLists[[input$GLSetMembers[1]]]
      if (length(input$GLSetMembers) > 1){
      for (i in 2:length(input$GLSetMembers)) {
        res <- switch(input$GLSetType,
                      Intersection = intersect(res, globalData$geneLists[[input$GLSetMembers[i]]]),
                      Union = union(res, globalData$geneLists[[input$GLSetMembers[i]]])
                      )
      }}
      globalData$geneLists[[input$geneListName]] <- res
      showshinyalert(session,"GLalert",HTMLtext="Gene list is now available",styleclass="success")
    }
  } else if (input$geneListType == "Import GO") {
    
    if (is.null(input$GLGOselect)) showshinyalert(session,"GLalert",HTMLtext=paste("No GO term selected"),styleclass="warning") else {
      goterm <- strsplit(input$GLGOselect,split="[\\(\\)]")[[1]]
      goterm <- goterm[grepl("GO", goterm)]
      genes <- checkInput(goterm,martf=martf[paste0(martf[,2], " (", martf$ensembl_gene_id,")") %in% allgenes[[globalData$activeDataSet]], ], uv = univariates[[globalData$activeDataSet]])
      if (is.null(genes)) showshinyalert(session,"GLalert",HTMLtext=paste("The selected GO term does not contain genes that are in the data"),styleclass="warning")else {
        globalData$geneLists[[input$geneListName]] <- genes
        showshinyalert(session,"GLalert",HTMLtext="Gene list is now available",styleclass="success")
      }
    }
  } else if (input$geneListType == "Literature") {
    file <- resources$file[resources$name == input$GLResource]
    data <- read.csv(file, header =T, stringsAsFactors=F)
    test <- by(data, as.factor(data$Group), function(x) unlist(sapply(x$Id, function(ensembl) checkInput(ensembl,martf=martf[paste0(martf[,2], " (", martf$ensembl_gene_id,")") %in% allgenes[[globalData$activeDataSet]], ], uv = univariates[[globalData$activeDataSet]]  ))))
    names(test) <- paste(input$geneListName,levels(as.factor(data$Group)),sep=".")
    globalData$geneLists <- c(globalData$geneLists, test)
    showshinyalert(session,"GLalert",HTMLtext="Gene list is now available",styleclass="success")
  }
      
      updateTextInput(session,"geneListName", value = "")
    }
  }
})

deleteGLitem <- observe({
  
  if(!is.null(input$GeneToDelete)) {
    if (input$GeneToDelete != globalData$GLDeleteCall) {
      input$GeneToDelete -> globalData$GLDeleteCall
    current_display <- isolate(input$GL2display)
    current <- isolate(globalData$geneLists[[current_display]])
    globalData$geneLists[[current_display]] <- current[current!=input$GeneToDelete]
  }
  }
})