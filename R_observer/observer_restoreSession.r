cbind_overwrite <- function(df1, df2) { #like cbind, but if a column name appears twice, only use the entry from df1
  use_df2 <- which( ! colnames(df2) %in% colnames(df1))

  cbind(df1, df2[,use_df2])
  
}


doRestore <- function(id, populationsOnly =F) {
  fname <- paste("sessions/",id,".r",sep="")
  if (!file.exists(fname)) {
    showshinyalert(session,"alert1",HTMLtext="No such state.",styleclass="warning") 
    return(0)
    } else {
    
    #load(fname)
    source(fname, local=T)
    
    if (!populationsOnly) {
    if (exists("plots")) {
      globalData$plots <- plots
      globalData$activePlot <- min(plots$id)
      updateSelectInput(session,inputId="pltType", selected=plots$type[1])#if (!exists("actds")) 
      updateSelectInput(session,inputId="onxaxis", selected=plots$x[1])
      updateSelectInput(session,inputId="onyaxis", selected=plots$y[1])
      updateSelectInput(session,inputId="xscale", selected=plots$x_scale[1])
      updateSelectInput(session,inputId="yscale", selected=plots$y_scale[1])
      globalData$plots$changed <- T
    }
    
      if (exists("actds")) { globalData$activeDataSet <- actds;} else { globalData$activeDataSet <- plots$type[1]} # updateSelectInput(session,inputId="pltType", selected=actds)
      if (exists("cg"))  globalData$currentGate <- cg
      if (exists("genelists")) globalData$geneLists <- genelists
      if (exists("colorscores")) globalData$colorScores <- colorscores
      if (exists("proj")) globalData$projections <- proj
      
      if(exists("kharchenko_populations")) {
        globalData$showKharchenko <- kharchenko_populations
        showshinyalert(session,"alert1",HTMLtext="To retrieve your results, move to the <b>Differential Expression</b> panel and press <b>Run!</b>",styleclass="success")
        #kharchenko$runCall <- kharchenko$runCall-1
      }
      
  }
      
      if (exists("gates")) 
        if (!is.data.frame(gates)) globalData$Gates <- gates #for downward compatibility, when gates were no fancy flowCore objects yet.
        
      
      oldcol <- sapply(populations, ncol)
      existcol <- sapply(globalData$Populations, ncol)
      r <- 2
      for ( i in 1:length(populations)) {
        if (ncol(globalData$Populations[[i]]) > 1) populations[[i]] <- cbind_overwrite(populations[[i]], globalData$Populations[[i]][,-1])
        if (ncol(populations[[1]]) != oldcol[1] + existcol[1] - 1) r <- 1
      }

      globalData$Populations <- populations
      return(r)
  }
}


restoreObs <- observe({
  if (input$restoreSession[1] > globalData$restoreStateCall) {
    globalData$restoreStateCall <- input$restoreSession[1]
    response <- doRestore(input$sessionIDin)
    if (response ==2 ) showshinyalert(session,"alert1",HTMLtext="Populations were restored successfully",styleclass="success") else if (response ==1) showshinyalert(session,"alert1",HTMLtext="Populations with identical names were overwritten",styleclass="warning")
  }
  
  if (nchar(query["state"]) > 5 & !globalData$stateRestored) {
    globalData$stateRestored <- T
    response <- doRestore(query["state"])
    if (response ==2 ) showshinyalert(session,"alert1",HTMLtext="Populations were restored successfully",styleclass="success") else if (response ==1) showshinyalert(session,"alert1",HTMLtext="Populations with identical names were overwritten",styleclass="warning")
#     fname <- paste("sessions/",query["state"],".rda",sep="")
#     if (!file.exists(fname)) showshinyalert(session,"alert1",HTMLtext="No such state.",styleclass="warning") else {
#       load(fname)
#       if (exists("plots")) {
#         globalData$plots <- plots
#         globalData$activePlot <- min(plots$id)
#         updateSelectInput(session,inputId="pltType", selected=plots$type[1])
#         updateSelectInput(session,inputId="onxaxis", selected=plots$x[1])
#         updateSelectInput(session,inputId="onyaxis", selected=plots$y[1])
#         updateSelectInput(session,inputId="xscale", selected=plots$x_scale[1])
#         updateSelectInput(session,inputId="yscale", selected=plots$y_scale[1])
#         globalData$plots$changed <- T
#       }
#       
#       if (exists("gates")) 
#         if (!is.data.frame(gates)) globalData$Gates <- gates
#       
#       if (ncol(globalData$Populations[[1]]) > 1) populations[[1]] <- cbind(populations[[1]], globalData$Populations[[1]][,-1])
#       if (ncol(globalData$Populations[[2]]) > 1) populations[[2]] <- cbind(populations[[2]], globalData$Populations[[2]][,-1])
#       globalData$Populations <- populations
#       
#       
#       if (exists("cg")) globalData$currentGate <- cg
#       
#       if (exists("genelists")) globalData$geneLists <- genelists
#       if (exists("colorscores")) globalData$colorScores <- colorscores
#       if (exists("proj")) globalData$projections <- proj
      
#       if(exists("kharchenko_populations")) {
#         globalData$showKharchenko <- kharchenko_populations
#         showshinyalert(session,"alert1",HTMLtext="To retrieve your results, move to the <b>Differential Expression</b> panel and press <b>Run!</b>",styleclass="success")
#         #kharchenko$runCall <- kharchenko$runCall-1
#       }
#}
  }
  
  if (input$restoreGates[1] > globalData$restoreGateCall) {
    globalData$restoreGateCall <- input$restoreGates[1] 
#     fname <- paste("sessions/",input$sessionIDin,".rda",sep="")
#     if (!file.exists(fname)) showshinyalert(session,"alert1",HTMLtext="No such state.",styleclass="warning") else {
#       load(fname)
#       #globalData$Gates <- gates
#       if (ncol(globalData$Populations[[1]]) > 1) populations[[1]] <- cbind(populations[[1]], globalData$Populations[[1]][,-1])
#       if (ncol(globalData$Populations[[2]]) > 1) populations[[2]] <- cbind(populations[[2]], globalData$Populations[[2]][,-1])
#       globalData$Populations <- populations
#       #globalData$currentGate <- cg
#       showshinyalert(session,"alert1",HTMLtext="Populations were restored successfully",styleclass="success")
#     }
    response <- doRestore(input$sessionIDin, populationsOnly=T)
    if (response ==2 ) showshinyalert(session,"alert1",HTMLtext="Populations were restored successfully",styleclass="success") else if (response ==1) showshinyalert(session,"alert1",HTMLtext="Populations with identical names were overwritten",styleclass="warning")
  }
},priority=1)

restoreObsFromJS <- observeEvent(input$triggerStateRestore, {
  response <- doRestore(input$triggerStateRestore)
  if (response ==2 ) showshinyalert(session,"alert1",HTMLtext="Populations were restored successfully",styleclass="success") else if (response ==1) showshinyalert(session,"alert1",HTMLtext="Populations with identical names were overwritten",styleclass="warning")
  
})
