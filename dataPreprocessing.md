### Single Cell RNA Seq Data Preprocessing for non-programmers

Before you start: Please create a Folder called preprocessing (e.g. on your Desktop), with subfolders transcriptomes,  raw, tmp, final, and counts
In the following, I will assume that all these folders are created on the Desktop of a Mac. If not, the paths need to be adjusted.

#### 1. Prepare Kallisto

When working on your own Mac or PC, please download the latest release of kallisto from https://pachterlab.github.io/kallisto/download.html , and install it according to https://pachterlab.github.io/kallisto/starting . On Mac, you need to 
* unpack the archive 
* open a terminal (Application -> Utilities -> Terminal) and change directory to the unpacked folder:
```
cd ~/Downloads/name_of_the_folder/
```
* Then, type:
```
sudo cp kallisto /usr/local/bin/
```
* You will be asked for your system's password.

You can check that the install was successful by opening a new terminal and typing kallisto

Then, download the file Homo_sapiens.GRCh38.rel79.cdna.all.fa.gz from http://bio.math.berkeley.edu/kallisto/transcriptomes/ to your folder transcriptomes

##### Build transcriptome index

Open a terminal and change directory to the transcriptomes folder. For example, if you have created the folder on your desktop, type

```
cd ~/Desktop/preprocessing/transcriptomes/
```

For kallisto to work, it needs a database of all k-mers occurring in the transcriptome. You can build it by typing

```
kallisto index -i human.idx Homo_sapiens.GRCh38.rel79.cdna.all.fa.gz
```


#### 2. Prepare R

Youy require the following software:
* R version 3.3 or greater from https://www.r-project.org
* R Studio from https://www.rstudio.com/products/rstudio/download/ 

Open RStudio and, please install the required packages

```
source("https://bioconductor.org/biocLite.R")
biocLite("scater")
biocLite("DESeq2")
biocLite("topGO")
biocLite("GO.db")
biocLite("flowCore")
biocLite("org.Hs.eg.db")
install.packages(c("sendmailR","plyr","scales","gsl","ggvis","colorRamps","ggplot2","BMS","plyr","reshape2","ggplot2","grid","shiny","devtools","docopt","feather"))
require(devtools)
install_github("AnalytixWare/ShinySky")
```

Finally, the scde package can be a bit rough to install on a Mac. Try

```
require(devtools)
install_github("hms-dbmi/scde")
```

If this fails, open a terminal and type

```
curl -O http://r.research.att.com/libs/gfortran-4.8.2-darwin13.tar.bz2
sudo tar fvxz gfortran-4.8.2-darwin13.tar.bz2 -C /
```

Then try again.

#### 3. Download indeXplorer
Go to https://git.embl.de/velten/indeXplorer 
Click “Repository”
Then press the download button on the top right and download as a zip archive
Unpack to a directory of your choice

#### 4. Prepare data for use with indeXplorer
Open the subfolder `dataPreparation` within your indeXplorer folder. There are 5 Scripts in there which you need to run in order to start indeXplorer

##### Perform pseudoalignment/counting
* Open `step0_kallisto2counts.r` with RStudio
* Adjust the paths from in the first lines of the Script to point to your  raw, tmp, final, and counts folders as well as to the transcriptome index you constructed earlier (in step 1 of this document). Also choose an output file (specify the full path, e.g. prepend folder/file names with `~/Desktop/preprocessing/transcriptomes/` !).
* ress “Source” in the top right corner to run the script. This should take less than 15 minutes

##### Perform quality control
* Open step0_dynamicQC.rmd with RStudio
* Simply press “Run Document”. A graphical interface will open. Select the output csv file created in the previous step.
* Several plots will appear and you can set several QC thresholds. The defaults are usually fine, however here:
* I subsampled each cell to only 100.000 reads to speed up processing. So you need to lower the “(aligned) reads per cell” and also the value n
* As we for this course only have 12 cells you might want to lower the number of cells each gene needs to be expressed by to 3 or 4.
* Press the download button at the bottom of the page to save the filtered dataset to a different csv file

##### Compute error models for differential expression testing
* Open step1_SCDE_errorModels.r with RStudio
* Adjust the parameters in the first lines. 
* Make sure that n.cores is set to 2 if you are running this on a Mac. 
* DATAFOLDER points to  ~/Desktop/preprocessing/final/
* Raw_data: Should contain only one entry (instead of two as in the template) and point to the csv file you created in the previous step.
* Press source
* Your folder final contains a file with cell-specific error models and a pdf file displaying the model fits

##### Normalize data
* There are two options for this, as I presented: “Pooling across Cells” from Lun, Bach & Marioni, Genome Biology 2016 and “Posterior Odds Ratios” from (the supplement of) Velten et al., Nature Cell Biology 2017
* For larger datasets , it may be easier (faster) for you to use Pooling Across Cells, which is implemented in the Script `step2A_optional_normalization_POR.r`
* If pooling across cells doesn't work (not enough cells to pool) and you can use `step2A_optional_normalization_POR.r`
* In either case, the parameters are the same as in the previous step.

##### Putting everything together
* Open step3_assemble.r
* Carefully read through the instructions in the first lines and adjust input values
* Let's not include FACS data at this stage, set these values to NULL
* Press source

##### Run indeXplorer
* In the main indeXplorer folder, open global.R.
* Set `DATAFOLDER` to your `final` folder
* Set `DEFAULTDATASET` to the name of your dataset (which was set in step1_SCDE_errorModels )
* As `DEFAULT_PLOT_X` set a gene of your choice e.g. `DNTT (ENSG00000107447)`
* As `DEFAULT_PLOT_Y` set a gene of your choice e.g. `MPO (ENSG00000005381)`
* Set `DEFAULT_SCALE_X` and `DEFAULT_SCALE_Y` to `linear`
* Press Run App (top right corner)
* **Happy browsing!**
