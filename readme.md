### About indeXplorer

indeXplorer is a web-based App written with [shiny](http://shiny.rstudio.com) for the explorative analysis of index-sorted, single-cell transcriptomic data. If you want to explore the hematopoietic stem cell dataset from [Velten, Haas, Raffel et al 2017](http://www.nature.com/doifinder/10.1038/ncb3493), you find a running instance of indexplorer [here](http://steinmetzlab.embl.de/shiny/indexplorer/). New users should consider starting with the [tutorial](http://steinmetzlab.embl.de/shiny/indexplorer/?demo=yes). 

This repository is for those who want to use indeXplorer to explore their own datasets, or who wish to contribute to the further development of the software. indeXplorer was designed for the use with index-sorted datasets, that is, datasets where for each cell, FACS index information is available in addition to gene expression data; in principle, it can be used with ordinary single-cell transcriptomic data also, but then, some of its features will be useless.

#### Requirements - Software

indeXplorer depends on a recent version of R (moved to 3.5.1) and multiple R/bioconductoR packages. These are listed in line 17ff. of `global.R` and include a developmental packages to be installed from [the shinysky github repository](https://github.com/AnalytixWare/ShinySky). Further, perl and the perl package `LWP::UserAgent` should be installed on your system.

The flowCore package has been updated since we developed indeXplorer in a way that is not compatible with large flowframes. We therefore include a fixed version of flowCore 1.46.2 as part of this repository. Please install the flowCore package from this folder.

We also provide a docker container registry for a shiny server with all packages to run  indeXplorer at https://git.embl.de/velten/rocker/tree/master/rocker/shiny/3.5.1-indeXplorer.

```
R version 3.5.1 (2018-07-02)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /g/easybuild/x86_64/CentOS/7/nehalem/software/OpenBLAS/0.2.20-GCC-6.4.0-2.28/lib/libopenblas_nehalemp-r0.2.20.so

locale:
 [1] LC_CTYPE=C                 LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
 [1] grid      parallel  stats4    stats     graphics  grDevices utils    
 [8] datasets  methods   base     

other attached packages:
 [1] org.Hs.eg.db_3.6.0          shinysky_0.1.2             
 [3] RJSONIO_1.3-0               reshape2_1.4.3             
 [5] flowCore_1.46.2             MAST_1.6.1                 
 [7] SingleCellExperiment_1.2.0  BMS_0.3.4                  
 [9] ggplot2_3.1.0               colorRamps_2.3             
[11] ggvis_0.4.3                 topGO_2.32.0               
[13] SparseM_1.77                GO.db_3.6.0                
[15] AnnotationDbi_1.42.1        graph_1.58.0               
[17] gsl_1.9-10.3                scales_0.5.0               
[19] plyr_1.8.4                  sendmailR_1.2-1            
[21] RColorBrewer_1.1-2          pheatmap_1.0.10            
[23] DESeq2_1.20.0               SummarizedExperiment_1.10.1
[25] DelayedArray_0.6.6          BiocParallel_1.14.2        
[27] matrixStats_0.54.0          Biobase_2.40.0             
[29] GenomicRanges_1.32.6        GenomeInfoDb_1.16.0        
[31] IRanges_2.14.11             S4Vectors_0.18.3           
[33] BiocGenerics_0.26.0         shiny_1.1.0                

loaded via a namespace (and not attached):
 [1] colorspace_1.3-2       htmlTable_1.12         corpcor_1.6.9         
 [4] XVector_0.20.0         base64enc_0.1-3        rstudioapi_0.7        
 [7] bit64_0.9-7            mvtnorm_1.0-8          splines_3.5.1         
[10] robustbase_0.93-0      geneplotter_1.58.0     knitr_1.20            
[13] Formula_1.2-3          jsonlite_1.5           Cairo_1.5-9           
[16] annotate_1.58.0        cluster_2.0.7-1        rrcov_1.4-4           
[19] compiler_3.5.1         backports_1.1.2        assertthat_0.2.0      
[22] Matrix_1.2-14          lazyeval_0.2.1         later_0.7.3           
[25] acepack_1.4.1          htmltools_0.3.6        tools_3.5.1           
[28] bindrcpp_0.2.2         gtable_0.2.0           glue_1.2.0            
[31] GenomeInfoDbData_1.1.0 dplyr_0.7.5            Rcpp_0.12.17          
[34] stringr_1.3.1          mime_0.5               XML_3.98-1.11         
[37] DEoptimR_1.0-8         zlibbioc_1.26.0        MASS_7.3-50           
[40] promises_1.0.1         memoise_1.1.0          gridExtra_2.3         
[43] rpart_4.1-13           latticeExtra_0.6-28    stringi_1.2.3         
[46] RSQLite_2.1.1          genefilter_1.62.0      pcaPP_1.9-73          
[49] checkmate_1.8.5        rlang_0.2.1            pkgconfig_2.0.1       
[52] bitops_1.0-6           lattice_0.20-35        purrr_0.2.5           
[55] bindr_0.1.1            htmlwidgets_1.2        bit_1.1-14            
[58] tidyselect_0.2.4       magrittr_1.5           R6_2.2.2              
[61] Hmisc_4.1-1            DBI_1.0.0              pillar_1.2.3          
[64] foreign_0.8-70         withr_2.1.2            survival_2.42-3       
[67] abind_1.4-5            RCurl_1.95-4.10        nnet_7.3-12           
[70] tibble_1.4.2           crayon_1.3.4           locfit_1.5-9.1        
[73] data.table_1.11.4      blob_1.1.1             digest_0.6.15         
[76] xtable_1.8-2           httpuv_1.4.4.1         munsell_0.5.0   
```

#### Requirements - Hardware

Depending on the size of your dataset, indeXplorer is handling some large data matrices, so you require at least several gigabytes of RAM. It runs fine on a Macbook Pro, but if you want to use the differential expression testing module, consider running it on a server with at least 5-10 cores.

#### Docker

A docker file is available at https://git.embl.de/velten/rocker/-/tree/indeXplorer/rocker/shiny/3.5.1/indeXplorer

#### Setting up your own indeXplorer server

To get started, all you need to do is the following:
* Download this repository
* Make sure all required R packages are installed on your system (see global.R)
* **Adjust some system-specific configuration in the first lines of `global.R`**
* For first use, we recommend that you download our processed data set at http://steinmetzlab.embl.de/shiny/indexplorer/data.zip, unpack to a directory of you choice, and specify that directory in global.R
* Start an R session and call:

```r
require(shiny)
runApp(port=5460) #use runApp(host="0.0.0.0", port=5460) to make it accessible to other computers on your network 
```

You will then find an instance of indeXplorer on http://127.0.0.1:5460 

Once you are happy with your setup of indeXplorer, you can consider hosting it via [Shiny Server](http://shiny.rstudio.com/articles/shiny-server.html).

indeXplorer is not meant to run under Windows.

#### Setting up your own data - Workflow from fastq files to a running instance (RNAseq only)

A complete workflow from raw fastq files to a running instance of indeXplorer, meant to be accessible to non-bioinformaticians, can be found [here](dataPreprocessing.md)

#### Setting up your own data - Detailed documentation

Raw and pre-processed data to be displayed in indeXplorer needs to be stored in a central data folder. We recommend that you download our data from https://crgcnag-my.sharepoint.com/:u:/g/personal/lvelten_crg_es/EVffx3I9MhlFljKwdTANCpcBhbMjEyRayaHoRxSoHA7NiA?e=AD9zMR to get started and to understand the underlying data formats.
```sh
unzip data.zip
```

To adapt you own data for the use with indeXplorer, we assume that you have the following:
* For each transcriptomic data set, a csv file of raw read counts with columns representing single cells and rows representing genes. The data set should already be filtered according to your QC standards. The rownames of the data should be ENSEMBL ids. The colnames of the data should be meaningful identifiers, we use the format plate&lt;plateid&gt;\_&lt;row&gt;\_&lt;column&gt; (for example, plate4_A_7). Spike-in data is not used by indeXplorer and should be excluded already.
* For each non-transcriptomic, e.g. functional data set, a csv file with columns representing single-cell features (e.g. total number of cells in colony) and rows representing single cells. The colnames of the data should be meaningful descriptors of the features, rownames should be e.g. plate&lt;plateid&gt;\_&lt;row&gt;\_&lt;column&gt;
* Highly recommended: For each data set, a csv file of flow cytometry data for the single-sorted cells. Rownames should be e.g. plate&lt;plateid&gt;\_&lt;row&gt;\_&lt;column&gt;. Column names should be meaningful (for example, CD34), and will be prepended FACS_ in indeXplorer. For BD cytometers, this file is readily generated from the FACS software. In case you are using a MoFlo-Astrios for index sorting, you need to generate this file from the FCS file using the [index_sorting R script by mandycm](https://github.com/mandycm/index_sorting). Please let me know if you experience problems with this workflow.  
* Optional: For each data set, A FCS file containing all (background) FACS data. We recommend some pre-gating (e.g. in our data set, we already excluded lineage positive cells) and subsampling to ~100.000 events, as plotting can be quite slow otherwise.
* Optional: A csv file containing any number of processed views on your data that you want to make accessible through indeXplorer. For example, you could pre-compute a t-SNE projection of your data, or even the raw read count per cell could be included here. Rownames should be plate&lt;plateid&gt;\_&lt;row&gt;\_&lt;column&gt;. Column names should be meaningful (e.g. tSNE_x).

CSV files should be separated by comma and contain colnames in the first row. Put differently, the R command
```r
data <- read.csv("filename", header=T, row.names=1)
```
needs to yield a valid data frame with rownames and colnames as described above.

For differential epxression analysis, indeXplorer relies on [scde](http://hms-dbmi.github.io/scde/index.html). To make computations faster, we pre-compute error models using the script dataPreparation/step1_SCDE_errorModels.r . Modify the first lines of the script to your needs and run it, preferably on a computing cluster with &gt;20 cores. For each transcriptomic dataset, this creates a file errormodels_DataSetName.rda

For most analyses (PCA, clustering, display of gene expression values) indeXplorer makes use of normalized, scaled gene expression values. We used Posterior Odds Ratios for this task, which is based on the error models from scde (see Methods of Velten et al., 2017). A good alternative is "pooling across cells" ( [Lun et al, Genome Biology 2016](http://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0947-7) ).
* If you wish to use posterior odds ratios, modify the first lines of the Script `dataPreparation/step2A_optional_normalization_POR.r` to your needs and run it, preferably on a computing cluster with >50 cores. This requires that scde error models have already been computed.
* For "pooling across cells", modify the first lines of the Script `dataPreparation/step2B_optional_normalization_PoolingAcrossCells.r` to your needs and run it
* In case you wish to use a different normalization method, we assume that this creates a matrix of normalized gene expression values. The rownames of the data should be exactly the same as in the raw data file. The colnames of the data should be plate&lt;plateid&gt;\_&lt;row&gt;\_&lt;column&gt; (for example, plate4_A_7). Store this matrix in a csv file.

Once all of the raw and processed data listed above is available, modify the script dataPreparation/step3_assemble.r to your needs and run it. This creates all the files required by indeXplorer.

Finally, the file ui.r contains hard-coded links to our default gating schemes (marked with a comment in that file), which are of course meaningless if you run indexplorer with your data, and loading them would fail. Once you have set up interesting gating schemes and plan to share your instance of indeXplorer with others, replace them.

### License
indeXplorer. Copyright (C) 2016-2019 Lars Velten

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.    
