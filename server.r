
shinyServer(function(input, output, clientData, session) {
  

  #Read input passed with the URL; there is a specific observer for query["state"]
  query <- parseQueryString(isolate(session$clientData$url_search))
  #if(query["pwd"] != "AML") stop("Unauthorized access")
  #if(query["demo"] != "") session$sendCustomMessage("RunTutorial",query["demo"])
  
  
  ####### Global Objects ######
  cat(query[["colors"]],"\n")
  if("colors" %in% names(query)) mycolors <- readRDS(query[["colors"]]) else mycolors <- ("a" = "green")
  #Major objects are
  #   - Plots: A data frame storing information about all plots the user created
  #   - Gates: A list of flowCore Gates
  #   - Populations: A logical data frame with as many rows as there are cells, as many columns
  #                  as there are populations. Creating a Gate also creates a population (storing
  #                  for each cell whether it's part of the Gate), but populations can also be
  #                  created differently, e.g. through clustering
  #   - Gene Lists: a list of vectors of genes
  # all of these are stored in a container called globalData, and changes in their values are monitored
  # by many reactive functions, e.g. the plotting function
  p <- list(); g <- list()
  for (ds in names(univariates)) {
    p[[ds]] <- data.frame(all = rep(T,nrow(univariates[[ds]])))
    g[[ds]] <- list()
  }
  
  globalData <- reactiveValues(
    Populations = p,
    Gates = g,
    activeDataSet = DEFAULTDATASET,
    currentGate = 0, #gate currently being drawn
    plots = data.frame(
      id = c(1),
      type = c(DEFAULTDATASET), 
      x = DEFAULT_PLOT_X,
      y = DEFAULT_PLOT_Y,
      x_scale = DEFAULT_SCALE_X,
      y_scale = DEFAULT_SCALE_Y,
      color_type = c("none"), 
      color_variable= c(NA), 
      color_scale = c(NA),
      todisplay = c("all"),
      tohide = c(""),
      gatingMode = c("AND"),
      changed = c(T), #this variable is monitored to trigger redrawing of the plots
      showDensity=c(F),
        stringsAsFactors=F
    ),
    geneLists = list(),
    projections = list(), #stores principle component results from the scatter plots module
    colorScores = list(), #a list to store the scores computed for mapping expression of a gene set as a color.
    activePlot = 1, #Plot that the user is currently working o
    makePlotCall = 0, #the following variables became necessary because I was unaware of observeEvent when I started writing.
    deletePlotCall = 0, #they just count how often every button was clicked to only trigger responses when the button is clicked again.
    updatePlotCall = 0,
    makeGateCall = 0,
    deleteGateCall = 0,
    updateGateCall = 0,
    restoreStateCall = 0,
    restoreGateCall = 0,
    GLCreateCall = 0,
    GLDeleteCall = "",
    GLDownloadCall = 0,
    heatmapPopCall = 0,
    HeatmapPopulation = NULL,
    dendrogram = NULL,
    heatmapData = NULL,
    heatmapDims = c(1,1),
    stateRestored = F,
    showKharchenko = NULL,
    grobs = list(NULL,NULL,NULL,NULL) #stores the final plot objects
  )

  #this container stores the positions of mouse clicks into the plots for creation of gates
  currentGate <- reactiveValues(
    x_plot1 = c(),
    y_plot1 = c(),
    x_plot2 = c(),
    y_plot2 = c(),
    x_plot3 = c(),
    y_plot3 = c(),
    x_plot4 = c(),
    y_plot4 = c()
  )
  
  #this container stores results from the differential expression analysis
  kharchenko <- reactiveValues(
    pop1 = NA,
    pop2 = NA,
    used_data = data.frame(),
    summary = data.frame(),
    result = data.frame(),
    prior = data.frame(),
    groups = factor(),
    models = data.frame(),
    go = data.frame(),
    runCall = 0,
    GOCall = 0,
    storeCall = 0,
    binding = F,
    job = NULL,
    id = ""
    )
  #this container stores results from the PCA module
  pca <- reactiveValues(
    scores = data.frame(),
    loadings = data.frame(),
    variance_observed = c(),
    variance_simulated = c(),
    runCall = 0,
    quickToken = "",
    labels = c(),
    binding = F,
    usecells = c(),
    totalRsqr = 0,
    storeCall = 0
    )
  
  #create a session ID for tmp data
  sID <- paste(sample(chars,size=4, replace=T),collapse="")
  
  ####### The scatter plots module (observers, functions and reactive plots) ###
  #The following script is the central piece of the module: It monitors whether any plot is changed
  #by the user and if so updates that plot. 
  source("R_functions/plotfunction.r",local=T) 
  source("R_functions/functions_ui.r", local=T) #minor functions to facilitate ui/server communication

  #The follwoing two four scripts monitor user input, adjust globalData and trigger plotfunction if required:
  observer <- observe({
    source("R_observer/observer_MakeDelSelect.r",local=T) #creation of new plots, selection of another plot as active plot
    source("R_observer/observer_adjustPlots.r", local=T) #user presses the update button - store user input in globalData and trigger plotfunction
  
    })
  
  activeData <- observe({
    if (!is.null(input$pltType)) {
    if (isTranscriptome [ input$pltType ] ) {
    globalData$activeDataSet <- input$pltType 
    showshinyalert(session,"alert1",HTMLtext=sprintf("All analyses from the PCA, Heatmap and Diff. Ex modules now<br>apply to the <b>%s</b> dataset", input$pltType),styleclass="success")
    } else showshinyalert(session,"alert1",HTMLtext=sprintf("All analyses from the PCA, Heatmap and Diff. Ex modules still<br>apply to the <b>%s</b> dataset", globalData$activeDataSet),styleclass="info")
    }
  })
  
  #one observer script for each module
  source("R_observer/observer_Gates.r",local=T) #user clicks or double clicks into plot to draw a gate.
  source("R_observer/observer_restoreSession.r",local=T) #restoring Sessions
  
  session_env <- environment()
  
  ###### Observers and reactive plots of further modules ######
  #the pca and scde (kharchenko) modules perform computations, store the results in a global reactiveValue 
  #and trigger drawing of the plots once computations are complete
  source("R_observer/observer_createGeneList.r", local=T)
  source("R_observer/observer_mast.r", local=T)
  source("R_observer/observer_pca.r", local=T)
  source("R_ui/ui_heatmap.r", local=T)
  #the heatmap module follows a more classical design, the heatmap plot directly reacts tp user input 
  
  ##### User interface. Creates reactive parts of the ui ####

    output$plotSelector <- renderUI({
      selectInput("pltNo", label="",choices=globalData$plots$id, selected = globalData$activePlot)
  })
  
 source("R_ui/ui_basic.r",local=T)
 source("R_ui/ui_color.r",local=T)
 source("R_ui/ui_gate.r", local=T)
 source("R_ui/ui_store.r",local=T)
 source("R_ui/ui_genelist.r", local=T)
  source("R_ui/ui_kharchenko.r", local = T)
  source("R_ui/ui_pca.r", local=T)

  
})
