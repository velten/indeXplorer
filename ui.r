#The basic UI is defined here, but many elements are reactive and are therefore defined in
#the scripts of the folder R_ui

#In five places marked with <-- HERE ,  our gating schemes are linked - remove if you use indexplorer with your own data

.libPaths("/home/shiny/rlibs")

library(shiny)
library(shinysky)
library(ggvis)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  tags$head(
    tags$style(HTML("
                    .popin {
                       width:600px;
	                    height:150px;
	                    margin: 0 auto;
                      padding: 10px;
	                    color: #000;
	                    background-color: #fff;
                      position: fixed;
                      top:100px;
                      left: 200px;
                      border-style: solid;
                      border-width: 4px;
                      z-index: 999;
                      display: none;
                    
}

                    "))
    ),  
  
  includeScript("tutorial.js"),
  # Application title
  titlePanel("indeXplorer"),
  tags$p("(c)", tags$a("Lars Velten", href = "mailto:velten@embl.de"), "Steinmetz lab, EMBL Heidelberg - ", tags$a("Launch tutorial", href = "#", onClick="return TutStart()"), " - Load default gating scheme:", tags$a("Individual 1", href = "#", onClick = "return loadState('defaultI1')"), " - ", tags$a("Individual 2", href = "#", onClick = "return loadState('defaultI2')"), " - ", tags$a("single cell culture", href = "#", onClick = "return loadState('defaultCulture')")), #<-- HERE
  HTML('  <div id="Tutorial", class = "popin">
		<div id = "TutBox">
		  <p id = "tutorialText"><b>Welcome to the indeXplorer tutorial.</b><br>This tutorial will show you how to explore single cell transcriptomic data by a few simple mouse clicks. Press Next to continue.</p>
      <a href="#" onClick="return TutNext()">Next</a>
			<a href="#" onClick="return hide(\'Tutorial\')">Quit</a>
       </div>
         </div>'),
  tabsetPanel(type = "tabs",
            tabPanel("Scatter Plots",
  #Start row1: Plots
  fluidRow(
      #create rows / columns dynamically according to the number of plots
    column(width=6,plotOutput("Scatter1", click = "sc1click",dblclick = "sc1dbl",inline=F),plotOutput("Scatter2", click = "sc2click",dblclick = "sc2dbl", inline=F)), #plots can send coordinates, but ggplots cannot
    column(width=6,plotOutput("Scatter3", click = "sc3click",dblclick = "sc3dbl",inline=F),plotOutput("Scatter4", click = "sc4click",dblclick = "sc4dbl", inline=F))
    ),
  hr(),
  fluidRow(
           column(width=2,busyIndicator("Updating Plots"), uiOutput("plotSelector")),
           column(width=3,actionButton("update", "Update"), actionButton("newPlot", "Create additional plot")),
           
           column(width=6, shinyalert("alert1", click.hide=T,auto.close.after=5))
  ),
  fluidRow(
      tabsetPanel(type="tabs",
                  tabPanel("Basic Setup", 
                           column(width=3, uiOutput("whattoplot")),
                           column(width=3,uiOutput("search")),
                           column(width=3,uiOutput("scale"))
                           ),
                  tabPanel("Colors",
                           column(width = 3, uiOutput("plotColor")),
                           column(width = 3, uiOutput("ColorSelection")),
                           column(width = 3, uiOutput("ColorSearch"))),
                  tabPanel("Populations", 
                           column(width=3 , uiOutput("groupSelection")),
                           column(width=3, uiOutput("gateSelector"),
                                  helpText("Gates are created by clicking into the plots. To create a rectangular gate, click once on the upper left corner, and close the gate by a double click in the lower right corner. For a polygonal gate, click on as many points as you wish and close the gate by double-clicking. To modify an existing gate, type the name of the gate you want to replace and draw it anew.")
                                  ),
                           column(width=3, uiOutput("hideSelection"))
                            #column(width = 6, uiOutput("gating"))
                  ),
                  tabPanel("Save & Restore",
                           tags$b("To store a state, click the button and remember the stateID that will be created"),
                           fluidRow(column(2, actionButton("storeSession", "Store State")),
                           column(3,verbatimTextOutput("sessionIDout"))), 
                           hr(),
                           tags$b("To restore a state, paste an exisiting stateID into the field below and click the button of your choice. Some useful predefined states:"),
                           tags$ul(
                             tags$li( tags$a("defaultI1", href = "#", onClick = "return loadState('defaultI1')"), "Contains all gates used in transcriptomics analysis, as well as the populations from the hierarchical clustering. Displays plots for individual 1."), # <-- HERE
                             tags$li( tags$a("defaultI2", href = "#", onClick = "return loadState('defaultI2')"), "Contains all gates used in transcriptomics analysis, as well as the populations from the hierarchical clustering. Displays plots for individual 2."), # <-- HERE
                             tags$li( tags$a("defaultCulture", href = "#", onClick = "return loadState('defaultCulture')"), "Contains gates and populations used in the single cell cultivation assay"), # <-- HERE
                             tags$li( tags$a("STEMNET", href = "#", onClick = "return loadState('STEMNET')"), "For each cell from the scTranscriptomics assay, contains classification: What CD38+ cell type does the cell ressemble most?") # <-- HERE
                             
                             ),
                           fluidRow(
                            column(3, textInput("sessionIDin",label="",value="")),
                           column(5,actionButton("restoreSession","Restore everything"),
                           actionButton("restoreGates","Restore Populations only"))),
                           hr(),
                           fluidRow(
                           column(2, downloadButton("saveFigures",label = "Store figures as pdf")),
                           column(3, textInput("figWidth",label="Width (inches)",value="3.2")),
                           column(3,textInput("figHeight",label="Height (inches)",value="2.4"))
                           )
                  )
    )
  
  )
                     
        ),
            tabPanel("PCA",
                     fluidRow(
                       uiOutput("PCAIntroduction")
                       ),
                     fluidRow(column(width=4, uiOutput("PCAPopulationSelect"),shinyalert("pcaalert", click.hide=T)),
                              column(width=4, uiOutput("PCAGeneListSelect")),
                              column(width=4, selectInput("PCAx","Princple component to plot on X", 1:10, 1),
                                     selectInput("PCAy","Princple component to plot on Y", 1:10, 2)
                              )),
                     
                     fluidRow(actionButton("PCARun",label="Run!")),
                     tabsetPanel(type = "tabs",
                                 tabPanel("Basic Plots",
                                      fluidRow(
                                               column(width=6, busyIndicator("Updating Plots"), ggvisOutput("ScoresPlot"), ggvisOutput("VariancePlot")),
                                               column(width=6, ggvisOutput("LoadingsPlot")))),
                                 tabPanel("Gene Ontology",
                                          helpText("This module performs gene ontology analysis of the genes with the 10% highest/lowest loadings. It may take a moment for the results to appear."),
                                          selectInput("PCAGOnumber","Select number of PCs to perform GO analysis for",1:10,4),
                                          uiOutput("PCAGO")
                                          ),
                                 tabPanel("Loadings Table",
                                          fluidRow(
                                            helpText("To create a gene list, select the number of genes to store and the principle component to use"),
                                            column(width=3, numericInput("PCAGeneListNumber", label="Create a gene list using the", value=100,min=1,max=5000)),
                                            column(width=3, selectInput("PCAGeneListTop", label="Top/Bottom genes", choices = c("Top","Bottom"), selected = "Top")),
                                            column(width=3, selectInput("PCAGeneListPC", label="on PC", choices = 1:8, selected = 1)),
                                            column(width=3, actionButton("PCAStoreGL","Create"))
                                            ),
                                          fluidRow(dataTableOutput("LoadingsTable"))
                                 ),
                                 tabPanel("Correlation with FACS markers",
                                          numericInput("PCAcorFDR", label="FDR threshold for correlations to display", value=0.01,min=0, max=1, step=0.01),
                     plotOutput("FACScorplot", inline=T)
                                 )
                     )
            )
            ,
            tabPanel("Heatmaps",
                     fluidRow(column(width =4,
                                     uiOutput("HeatmapPopulationSelect")),
                              column(width=4,
                                     uiOutput("HeatmapGeneSelect")),
                              column(width=4,
                                     #checkboxInput("HeatmapCluster","Cluster only based on selected genes", T),
                                     #helpText("Note: Here, a standard hierarchical clustering algorithm is used to provide a convincing visual display. My plan is to create documents during preprocessing that cluster genes and cells into statistically sound numbers of clusters."),
                                     selectInput("Heatmap.method", "Clustering Method", choices = c("average", "complete", "single", "ward"), selected = "ward"),
                                     selectInput("Heatmap.distCells","Distance Metric (Cells)", choices = c("Euclidean","Correlation"), selected="Euclidean"),
                                     selectInput("Heatmap.distGenes", "Distance Metric (Genes)", choices = c("Euclidean","Correlation"), selected="Euclidean"),
                                     numericInput("Heatmap.nCor","If only one gene is selected, display the",value=10,step=1,min=3,max=100),
                                     selectInput("Heatmap.modeCor","genes with the highest", choices=c("absolute","positive","negative"), selected="absolute"),
                                     selectInput("Heatmap.methodCor","",choices=c("Pearson","Spearman"),selected="Pearson"),
                                     tags$p("Correlation")
                                     )),
                     fluidRow(actionButton("HeatmapMake",label="Create Heatmap"),
                              actionButton("HeatmapPop", label="Store populations"),
                              shinyalert("HMalert", click.hide=T,auto.close.after=5)
                     ),
                     fluidRow(plotOutput("Heatmap",inline=T))
                     ),
            tabPanel("Differential Expression",
                     fluidRow(
                       p(tags$b("This is an interface to the MAST package described by Finak et al (", tags$a("pubmed", href = "http://www.ncbi.nlm.nih.gov/pubmed/26653891", target="_blank"), ") which allows you to identify genes with differenial expression among arbitrary cell populations.")),
                       p("Computations take between one and 10 minutes depending on the number of cells to be analyzed.")
                       ),
                     fluidRow(
                       column(width = 4, uiOutput("KharchenkoPopulationSelect"),
                              actionButton("KharchenkoRun",label="Run!"),
                              shinyalert("KharchenkoAlert", click.hide=F),
                              busyIndicator("Updating Plots")),
                       column(width = 4, p(tags$b("Display options")),
                              helpText("Changing these values changes the display, without re-running the analysis."),
                              uiOutput("KharchenkoGLSelect"),
                              numericInput("KharchenkoFCThresh", label="Select FDR threshold for display", value=0.1,min=0.0,max=1,step=0.01)
                              )#,
                        #column(width=4, tags$b("Advanced Options"),
                        #      checkboxInput("KharchenkoThresholdSegmentation",label="Preload errormodels that were computed on all cells jointly , instead of the two populations you are looking at separately (saves computing time, but not 100% correct)",value = T),
                        #       checkboxInput("KharchenkoStore", label="If same populations have been analyzed before, do not rerun analysis.", value=T)
                        #       )
                     ),
                     hr(),
                     fluidRow(
                       column(width= 6, ggvisOutput("KharchenkoPlot"))#,
                       #column(width=6,  textInput("KharchenkoDetailGene", label="Select gene to view in detail", value=""),
                       #                 plotOutput("KharchenkoDetailPlot")
                       #)
                     ),
                     fluidRow(p(id="KharchenkoFurther",tags$b("Further analysis")),
                              selectInput("KharchenkoGenes2Store", label = "These analyses can be performed on all hits, upregulated hits, or downregulated hits", choices=c("All", "Upregulated", "Downregulated"), selected = "All"),
                              helpText("You can create a gene list from all genes that are displayed as significant in the plot. Gene lists can be further analyzed in the Gene lists panel"),
                              
                              actionButton("KharchenkoStoreGL", "Create Gene List"),
                              helpText("You can perform a Gene Ontology enrichment on the genes that are displayed as significant."),
                              actionButton("KharchenkoGO","Run GO enrichment"),
                              shinyalert("KharchenkoGOalert", click.hide=T)
                     ),
                     fluidRow(#selectInput("KharchenkoTableToDisplay",label="Select Result to display (requires that analyses were performed)", choice = c("Genes","Gene Ontology"), selected = "Genes"),
                              dataTableOutput("KharchenkoData"),
                              downloadButton("KharchenkoDownload","Download Result")
                     )
            ),
            tabPanel("Gene Lists",
                              p(tags$b("Gene lists are created whenever you perform an analysis. You can also import gene lists through file upload, from Gene Ontology or from select literature resources.")),
                              p(tags$b("You can use Gene Lists to plot mean expression on scatter plots, to create heat maps, to run clustering algorithms or PCA only on some genes etc.")),
                              shinyalert("GLalert", click.hide=T,auto.close.after=5),
                              fluidRow(column(width=2,selectInput("geneListType",label="Create new gene list through", c("File Upload","Import GO","Literature", "Set operation"), selected = "File Upload")),
                              column(width=2,textInput("geneListName",label="Unique name for the gene List"),
                                     actionButton("createGeneList", "Create")),
                              column(width=3,uiOutput("geneListCreate")),
                              column(width=5,uiOutput("GLgoSelect"))         
                              ),
                              hr(),
                              uiOutput("geneListSelector"),
                              uiOutput("geneListDisplay"),
                              downloadButton("GLdownload", "Download Genelist")
                              
               )
                              
)

))