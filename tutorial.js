//https://ryouready.wordpress.com/2013/11/20/sending-data-from-client-to-server-and-back-using-shiny/
var tutorialState = 0;
var tutorialOK = true;
var tutorialText = new Array("<b>Welcome to the indeXplorer tutorial.</b><br>This tutorial will show you how to explore single cell transcriptomic data by a few simple mouse clicks. Press Next to continue.",
"You are currently in the <b>Scatter plot</b> module. This is the most powerful module, but we'll start simple. At the moment, you see one scatter plot of single cells, which displays the FACS index values of cd34 and cd38.",
"Let's define a population of common lymphoid progenitors. This cell type is defined as <i>cd38+cd10+</i>. Scroll down and change the plot to display cd38 on the x axis and cd10 on the y axis. Also, change the <i>Dataset to plot</i> to <b>Individual 2</b>, as this dataset is more interesting with regard to lymphoid progenitors. ",
"Once you enter a search string, the dropdown list on the right will show all matching variables. For example, the search string cd38 matches both the FACS surface marker cd38, as well as the CD38 transcript. For now, let's choose the FACS variable.",
"In order to update the plot, press the update button.",
"As you can see, there are both <i>cd38<sup>high</sup></i> and <i>cd38<sup>low</sup></i> cells among the <i>cd10+</i> population. Let's plot the expression of the <i>DNTT</i> gene onto the same plot as a colorscale. To do so, navigate to the <b>Colors</b> panel.",
"This panel offers many different types of color displays. In order to plot a gene expression value, select <i>univariate</i>, then, search for the DNTT gene. Once you are sure to have chosen the correct gene, press update.",
"As you can see, the <i>cd38<sup>high</sup>cd10+</i> population expresses <i>DNTT</i>, whereas the <i>cd38<sup>low</sup>cd10+</i> population has not yet started that stage of Ig locus rearrangement. Let's explore the DNTT-positive cells further. To do so, navigate to the <b>Populations</b> panel.",
"Try setting a <i>CD38+cd10+</i> gate that captures most of the <i>DNTT</i> expressing cells. To do so, choose a name for the gate, then click into the plot once at the upper left corner of the desired gate, and close the gate by double clicking in the lower right corner.",
"The gate is displayed on the scatter plot. We can now investigate the <i>CD38+cd10+</i> population further using hierarchical clustering. To do so, navigate to the <b>Heatmaps</b> module.",
"Select the <i>CD38+cd10+</i> population that you have just defined as the population to display. Do not assign cell labels, set <i>genes to display</i> to 30, and leave all other values unchanged: The clustering will be performed on the 1000 most variable genes. Press <b>Create Heatmap</b> to perform the clustering, which will take some instants.",
"If you have chosen the correct population, you should see that the <i>cd38+cd10+</i> cells form two very distinct clusters, one expressing CD9 and the other expressing IL7R. We can also define populations using clustering. To do so, set <b>Clusters to cut tree into</b> to 2, and again press <b>Create Heatmap</b>",
"If you are happy with the result, press <b>Store Populations</b>. Then, we can try to explore gating strategies to separate these two populations a priori. To do so, move back to the <b>Scatter Plots</b> panel.",
"Let's highlight the result of the clustering on the cd38 vs. cd10 scatter plot. To do so, navigate to the <b>Colors<b/> panel.",
"This time, select <b>Population</b>, then tick <i>Heatmap1-1</i> and <i>Heatmap1-2</i> and press <b>Update</b>.",
"As you can see, one of the two populations expresses cd10 to higher levels than the other. Now, create a second plot by pressing <b>Create additional plot</b>, and then select plot 2 in the plot selector dropdown list.",
"Then, navigate to the <b>Populations</b> panel.",
"You can display any number of populations in a scatter plot. Untick <i>all</i> and tick your <i>cd38+cd10+</i> population. Pess <b>Update</b> to commit the changes.",
"Try to change plot 2 to display <i>IL7R</i> gene expression on the x axis, <i>FSC-A</i> (forward scatter) on the y axis and again use the populations from hierarchical clustering as color coding.",
"As you see, the populations differ in IL7R exprsession and cell size. Happy with the results? To save your work, navigate to the <b>Save & Restore</b> panel.",
"Press the <b>Store State</b> button and copy the 20-letter ID that is created to your clipboard. You can recover your work either using this panel, or by using the URL http://steinmetzlab.embl.de/shiny/indexplorer/?state=your-sate-id .",
"Load our standard gating scheme by typing <b>defaultI2</b> into the highlighted field, then press <b>Restore Populations only</b>. If you have named your population CD38+cd10+, it will be overwritten.",
"Now, let's look at the <b>Differential Expression</b> module.",
"Select the <i>CD38+cd10+</i> as the population to test for, and <i>all</i> as the population to test against. Please make sure to use our predefined <b>CD38+cd10+</b> population, as this modules stores previous computations (by any user). If you run it for a new population that has not yet been analyzed before, analysis may take up to an hour depending on how busy our server is. Finally, press <b>Run!</b>",
"In the plot, you can hover over individual points (genes) to learn their identity. You can also click on points to view the cell-wise posteriors for the gene.",
"In the bottom of the page, you find a queriable table of the gene expression result which can be downloaded as a csv file using the <b>Download Result</b> button at the very bottom.",
"Now, try to export the Upregulated hits to the Gene Lists module. To do so, under <b>Further analysis</b>, select <b>Upregulated</b>, and press <b>Create Gene List</b>. Then, navigate to the Gene Lists module.",
"You see the complete list of hits in a format that allows you to quickly obtain further information about each gene.",
"Let's try to identify surface markers that are upregulated in the <i>cd38+cd10+</i> population. To do so, you have to create a new gene list that contains all surface markers. In <b>Create new gene list through</b>, select <i>Import GO</i>, choose a unique name, then search for <i>cell surface</i> and select <i>cell surface (GO:0009986)</i> from the drop-down list. Finally, press <b>Create</b>",
"Now, intersect the list of upregulated genes with the list of surface markers by selecting <i>Set operation</i> in <b>Create new gene list through</b>. Make sure to choose a new name.",
"You can view the new gene list by selecting it from the gene list selector dropdown menu.",
"Finally, please create a new gene list of cell cycle genes (gene ontology <i>mitotic cell cycle (GO:0000278)</i>). We will need it in the next step.",
"Finally, let's explore the <b>PCA</b> module.",
"Perform a PCA on the <i>CD38+cd10+</i> population, and choose to highlight your <i>heatmap1</i> populations. Also, choose to highlight the cell cycle gene list on the loadings plot. Finally, press <b>Run!</b>. Computations will take some moments.",
"Once the computations are complete, you explore the scores and loadings plot. If you click on a gene in the loadings plot, you will be redirected to the gene card site for said gene. As you should see from the loadings plot, the two populations differ with regard to cell cycle activity. The IL7R+ populations is more actively cycling.",
"The <b>Loading Table</b> panel allows you to browse the loadings, and to create gene lists from the Loadings table. Further possibilities of deeper analysis include Gene Ontology and correlations of principle components with FACS markers.",
"While this module offers many tools for detailed analysis of PCA loadings, you can also plot principle components in the <b>Scatter Plot</b> modules, for example if you want to set Gates in principle component space or if you want to plot PCA scores as a color score on FACS plots.",
"Thank you for taking your time for this Tutorial. We hope that indeXplorer will be useful for your research. If you click <i>Next</i>, we will demonstrate how indexplorer can be used to find stem cell quiescence markers.",
"<b>Welcome to indeXplorer</b>. This small demo will show how indeXplorer was used to identify CD44 as an additional marker for human HSC quiescence. For the main Tutorial, see <a href = '#', onClick='return TutStart()'>here.</a>",
"Press <i>next</i> again to load our default gating scheme for individual 1.",
"Please wait for the plots to appear.",
"Now, navigate to the <i>Gene Lists</i> module.",
"You need to create two new gene lists through <i>Import from GO</i>. Look for GO:0007049 in the <i>Free search of GO terms and IDs</i> field, and choose CellCycle as the <i>Unique name for the gene list</i>. Then, press create.",
"Once the gene list is loaded, create a second gene list called PlasmaMembrane from GO:0005887",
"We are now interested in CD34+CD38- stem cells. To analyze their heterogeneity further, navigate to the <i>PCA</i> module.",
"Select to run the PCA only on CD38- cells. Press run and wait for the results to appear.",
"Highlight cell cycle genes by choosing the cell cycle gene list in the <i>Select a gene list to highlight on the loadings plot</i> box.<br>",
"The loading plot now shows that cells with a negative score on PC1 are more active in cell cycle. To confirm this, you can navigate to the <i>Gene Ontology</i> submodule.",
"GO analysis takes a moment; once the result becomes available, look at the GO result for PC1, bottom scores. Cell cycle is the top hit.",
"To identify markers for the most quiescent cells, navigate to the <i>Loadings Table</i> submodule. Create a gene list containing the 200 genes with the <b>highest</b> scores.",
"Navigate back to the gene lists module",
"Create a gene list through set operation by an intersection between the gene list from your PC1, and the PlasmaMembrane gene list. Display the gene list.",
"One of the genes with higher expression on quiescent stem cells is CD44. Hoechst/Ki67 staining of CD44+ cells offers experimental confirmation: See <a href='cd44.png' target='_blank'>here</a>",
"<b>Welcome to indeXplorer</b>. <a href = '#', onClick='return TutStart()'>Take me to the main tutorial.</a>"
);
var tutorialX = new Array('400px','400px','600px','600px','600px', '600px', '600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px','600px');
var tutorialY = new Array('200px','300px','400px','400px','400px','300px','400px','300px','400px','200px','200px','200px','200px','400px','400px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','500px','500px','300px','300px','400px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px','300px');
var highlightObject = new Array('', 'test1-container', 'whattoplot', 'search', 'update','','plotColor','','gating','','HeatmapPopulationSelect','HeatmapPopulationSelect','HeatmapPop','','plotColor','plotSelector','','populations2show','','','sessionIDout','sessionIDin','','KharchenkoPopulationSelect','','KharchenkoDownload','KharchenkoFurther','','','','geneListSelector','','','PCAPopulationSelect','','','','','','','LOADPOP1','','PCAPopulationSelect','PCAGeneListSelect','','','','','','');


var check =  setInterval(function(){ Shiny.onInputChange("checkForResults", Math.random()); }, 10000);

    
//setInterval(function() { Shiny.onInputChange("checkForResults", Math.random()); }, 30000);

function passID(id) {
Shiny.onInputChange("GeneToDelete", id);
}

function loadState(id) {
Shiny.onInputChange("triggerStateRestore", id)
}

Shiny.addCustomMessageHandler ("OpenGencards",
   function(gene) {
       url = "http://www.genecards.org/cgi-bin/carddisp.pl?gene=" + gene
       window.open(url, '_blank')
       }
    
)

Shiny.addCustomMessageHandler ("DisplayFigure",
            function(image) {
                window.open(image, 'figuredisplay', width=640, height=640)
                }
)

Shiny.addCustomMessageHandler ("RunTutorial",
            function(what) {
                    
                    if (what == "yes" | what == "main" | what == "Bcell") {
                    tutorialState = 0;
                    } else if (what == "HSC") {
                        tutorialState = 38;
                    } else {
                            tutorialState = 48;
                    }
                    
                    document.getElementById('Tutorial').style.display='block';
                    document.getElementById('tutorialText').innerHTML = tutorialText[tutorialState];
                    document.getElementById('Tutorial').style.top = tutorialY[tutorialState];
                    document.getElementById('Tutorial').style.left = tutorialX[tutorialState];
                    return false;
                }
)

function TutStart() {
	document.getElementById('Tutorial').style.display='block';
        tutorialState = 0;
            document.getElementById('tutorialText').innerHTML = tutorialText[tutorialState];
        document.getElementById('Tutorial').style.top = tutorialY[tutorialState];
        document.getElementById('Tutorial').style.left = tutorialX[tutorialState];
	return false;
}


function TutNext() {
        
        if (tutorialState < 53) {
        tutorialState++;

        document.getElementById('tutorialText').innerHTML = tutorialText[tutorialState];
        document.getElementById('Tutorial').style.top = tutorialY[tutorialState];
        document.getElementById('Tutorial').style.left = tutorialX[tutorialState];
        if (highlightObject[tutorialState] == 'LOADPOP1') {
                loadState('defaultI1');
             } else if (highlightObject[tutorialState] != '') {
                document.getElementById(highlightObject[tutorialState]).style.borderStyle = 'solid';
                document.getElementById(highlightObject[tutorialState]).style.borderWidth = '4px';
                document.getElementById(highlightObject[tutorialState]).style.borderColor = '#FF0000';
            }
        if (tutorialState > 0) {
            if (highlightObject[tutorialState-1] != '') {
            document.getElementById(highlightObject[tutorialState-1]).style.borderStyle = 'none';
            }
            }
        } else {
            hide('Tutorial')
            }
        
        return false;
}

function hide(div) {
    
    if (tutorialState > 0) {
            if (highlightObject[tutorialState-1] != '') {
            document.getElementById(highlightObject[tutorialState-1]).style.borderStyle = 'none';
            }
        }
	document.getElementById('Tutorial').style.display='none';
	return false;
}