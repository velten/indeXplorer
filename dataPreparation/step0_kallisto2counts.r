
#SETUP
#0. Required libraries
require(scater)

n_cores <- 2 #adjust to your system!

#1. If you have a paired end experiments, supply a path containing files with names 
#      following the pattern: CellID-fwd/rev.fastq.gz
#      e.g. I1_plate4_A_1-fwd.fastq.gz, I1_plate4_A_1-rev.fastq.gz
#   Or, if you have a single end experiment, supply a path containing files with
#      names following the pattern CellID.fastq.gz
raw_data <- "/g/steinmetz/velten/Data/Niche/Kunisaki2013/raw/"

#2. supply a path to store kallisto output
kallisto_output <- "/g/steinmetz/velten/Data/Niche/Kunisaki2013/kallisto_out/"

#3. supply name of a csv file to store final count data
final_output <- "/g/steinmetz/velten/Data/Niche/Kunisaki2013/kallisto.csv"

#4. Supply a file containing kallisto transcriptome index
#   see https://pachterlab.github.io/kallisto/starting on how to build one 
#   and http://bio.math.berkeley.edu/kallisto/transcriptomes/ for useful files
tx_index <- "/g/steinmetz/genome/Mus_musculus/38.91/Mouse.38.91.idx"

#5. Specify a tmp dir
tmp_dir <- "/g/steinmetz/velten/Data/Niche/Kunisaki2013/tmp/"

#6. parameters for kallisto
fragment_length <- 350
fragment_deviation <- 100

#7. specfiy the species (one of: human, mouse, fly)
SPECIES <- "human"


#COMMANDS
#1. create kallisto targets file
paired <- list.files(raw_data,pattern = "-fwd.fastq.gz", full.names = F)
unpaired <- list.files(raw_data,pattern = ".fastq.gz", full.names = F)
unpaired <- unpaired[!(grepl("-fwd.fastq.gz",unpaired) | grepl("-rev.fastq.gz",unpaired))]

if(length(unpaired) == 0 & length(paired) == 0) stop("The folder", raw_data, "does not contain .fastq.gz files")
if(length(unpaired) > 0 & length(paired) > 0) stop("The folder", raw_data, "contains both files indicative of paired-end sequencing (ending in fwd.fastq.gz or rev.fastq.gz) and files indicative of single-end sequencing (ending in fastq.gz). Please supply only one type of experiment")


if (length(paired) > 0 ){
  pos <- regexpr("-fwd.fastq.gz",paired)
  cellID <- substr(paired, 1,pos-1)
  rev <- paste0(cellID,"-rev.fastq.gz")
  files_fwd <- file.path(raw_data, paired)
  files_rev <- file.path(raw_data,rev)
  if (!all(file.exists(files_rev))) stop("The following fwd files (-fwd.fastq.gz) miss a corresponding rev file for a paired end sequencing experiment")
  samples <- data.frame(cellID, paired, rev)
}

if (length(unpaired) > 0 ){
  pos <- regexpr("fastq.gz",unpaired)
  cellID <- substr(unpaired, 1,pos-1)
  samples <- data.frame(cellID, unpaired)
}

write.table(samples,
            file.path(raw_data, "samples.txt"),
            quote=F, row.names=F, col.names=T ,sep="\t")

kallisto <- runKallisto(targets_file =  file.path(raw_data, "samples.txt"),
                        transcript_index=tx_index,
                        single_end = length(unpaired) > 0,
                        output_prefix = kallisto_output,
                        fragment_length = fragment_length,
                        fragment_standard_deviation = fragment_deviation,
                        n_cores = n_cores)



    ## Read kallisto results in R
    sce_tx <- readKallistoResults(kallisto, read_h5 = TRUE)

    ## Check results
    if ( any(apply(assays(sce_tx)$exprs, 2,
    function(x) {any(is.na(x))})) ) {
        warning("The following samples have NA expression values")
        ww <- which(apply(assays(sce_tx)$exprs, 2,
        function(x) {any(is.na(x))}))
        cat(sampleNames(sce_tx)[ww])
    }

    ## Get transcript feature annotations from biomaRt
    dataset <- switch(SPECIES, human = "hsapiens_gene_ensembl",
                      mouse = "mmusculus_gene_ensembl",
                      fly = "dmelanogaster_gene_ensembl")
    genename <- switch(SPECIES, human = "hgnc_symbol",
                       mouse = "mgi_symbol",
                       fly = "external_gene_name")
    
    sce_tx <- getBMFeatureAnnos(
        sce_tx, filters = "ensembl_transcript_id",
        attributes = c("ensembl_transcript_id", "ensembl_gene_id",
        genename, "chromosome_name", "transcript_biotype",
        "transcript_start", "transcript_end", "transcript_count"),
        feature_symbol = genename, feature_id = "ensembl_gene_id",
        dataset = dataset,host = "www.ensembl.org")

    ## tweak feature names to be more readable
    #featureNames(sce_tx) <- paste(
    #    fData(sce_tx)$hgnc_symbol, " (",featureNames(sce_tx), ")",sep = "")

    ## Save SCESet object
#     if ( verbose ) {
#         cat("Saving transcript expression object to: ")
#         cat(paste0(output_prefix, "_tx.rds"))
#         cat("\n")
#     }
#     saveRDS(sce_tx, file = paste0(output_prefix, "_tx.rds"))

    ## If number of samples is large, save tx into multiple rds files
    ## each containing a max of 500 samples
    idx_list <- split(colnames(sce_tx), ceiling(seq_len(ncol(sce_tx)) / 500))
    tmp_file_list <- paste0(tmp_dir, "_tmp_tx_", 1:length(idx_list), ".rds")
    for (i in seq_along(idx_list)) {
        saveRDS(sce_tx[, idx_list[[i]]], file = tmp_file_list[i])
    }
    cat("Removing transcript expression object...")
    rm(sce_tx)
        
    ## Summarise expression at gene level
    cat("Summarising transcript expression at gene level...\n")
    tmp_tx <- readRDS(tmp_file_list[1])
    sce_gene <- summariseExprsAcrossFeatures(tmp_tx)
    if (length(idx_list) > 1) {
      for (i in 2:length(idx_list)) {
        cat("File ", i, " of ", length(idx_list), "\n")
        tmp_tx <- readRDS(tmp_file_list[i])
        tmp_gene <- summariseExprsAcrossFeatures(tmp_tx)
        sce_gene <- mergeSCESet(sce_gene, tmp_gene)
    }
}
    ## remove temporary files
    for (i in seq_along(idx_list)) {
        rm_cmd <- paste("rm", tmp_file_list[i])
        system(rm_cmd)
    }

   write.csv(round(assays(sce_gene)$counts), file=final_output, quote=F)

