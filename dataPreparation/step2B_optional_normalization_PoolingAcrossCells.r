#1. Package requiremnts
require(scater)
require(scran)

#2. specify the folder that processed data should be written to
#   This should be the same folder specified in global.R for use with indeXplorer
#   Also, error models computed by step1_SCDE_errorModels.r should be contained in that folder
DATAFOLDER <- "/path/to/data"

#3. Specify the raw data files: 
#   For each transcriptomic data set, a matrix of raw read counts with columns representing single cells and rows representing genes. 
#   The data set should already be filtered according to your QC standards. 
#   The rownames of the data should be ENSEMBL ids. The colnames of the data should be plate<plateid>_<row>_<column> (for example, plate4_A_7). 
#   Spike-in data is not used by indeXplorer and should be excluded already.
#   choose Data Set Names to your liking
raw_data <- list(DataSetName1 = "path/fileName1.csv", DataSetName2 = "path/fileName2.csv")

#Please check the sizes in line 33 (computeSumFactors)

for (ds in names(raw_data)) {
  cd <- read.csv(raw_data[[ds]], header=T, row.names = 1)
  sg <- as.factor(rep("all", ncol(cd)))
  names(sg) <- colnames(cd)
  cd<- cd[rowSums(cd) > 0,] #sanity check to remove non-expressed genes
  #data set should already be filtered!!!
  sce <- newSCESet(countData=CELLS)
  #pheatmap(cor(CELLS), annotation_col = QC, show_rownames = F, show_colnames = F)
  cluster <- quickCluster(sce, min.size=50)
  sce <- computeSumFactors(sce ,sizes = c(10,20,30,40), cluster=cluster)
  sce <- normalize(sce)
  NORMALIZED <- sce@assayData$norm_exprs
  
  save(normalized, file=sprintf("%s/POR_%s.rda",DATAFOLDER, ds))
}