#1. Package requiremnts
require(scde)

#2. Specify number of cores to be used
n.cores <- 50

#3. specify the folder that processed data should be written to
#   This should be the same folder specified in global.R for use with indeXplorer
DATAFOLDER <- "/path/to/data"

#4. Specify the raw data files: 
#   For each transcriptomic data set, a matrix of raw read counts with columns representing single cells and rows representing genes. 
#   The data set should already be filtered according to your QC standards. 
#   The rownames of the data should be ENSEMBL ids. The colnames of the data should be plate<plateid>_<row>_<column> (for example, plate4_A_7). 
#   Spike-in data is not used by indeXplorer and should be excluded already.
#   choose Data Set Names to your liking
raw_data <- list(DataSetName1 = "path/fileName1.csv", DataSetName2 = "path/fileName2.csv")


setwd(DATAFOLDER)
for (ds in names(raw_data)) {
  cd <- read.csv(raw_data[[ds]], header=T,row.names = 1)
  sg <- as.factor(rep("all", ncol(cd)))
  names(sg) <- colnames(cd)
  cd<- cd[rowSums(cd) > 0,] #sanity check to remove non-expressed genes
                            #data set should already be filtered!!!
  
  cat("Now fitting error models for data set", ds, "\n")
  o.ifm <- scde.error.models(counts=cd,groups=sg,n.cores=n.cores,threshold.segmentation=F,save.crossfit.plots=F,save.model.plots=T,verbose=1);
  
  #filters on o.ifm to exclude cells with a poor error model fit from (all) downstream analyses
  #at least check that correlation of observed and expected gene expression levels is positive
  o.ifm <- subset(o.ifm, corr.a > 0)
  #optionaly implement further filters on the dispersion
  #o.ifm <- subset(o.ifm, corr.theta > thrsh)
  
  save(o.ifm, file=sprintf("%s/errormodels_%s.rda",DATAFOLDER, ds))
}




