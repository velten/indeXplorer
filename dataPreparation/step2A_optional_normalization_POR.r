#1. Package requiremnts
require(scde)
require(parallel)


#2. Specify number of cores to be used (numerical integration is memory intensive)
n.cores <- 5

#3. specify the folder that processed data should be written to
#   This should be the same folder specified in global.R for use with indeXplorer
#   Also, error models computed by step1_SCDE_errorModels.r should be contained in that folder
DATAFOLDER <- "/path/to/data"

#4. Specify the raw data files: 
#   For each transcriptomic data set, a matrix of raw read counts with columns representing single cells and rows representing genes. 
#   The data set should already be filtered according to your QC standards. 
#   The rownames of the data should be ENSEMBL ids. The colnames of the data should be plate<plateid>_<row>_<column> (for example, plate4_A_7). 
#   Spike-in data is not used by indeXplorer and should be excluded already.
#   choose Data Set Names to your liking
raw_data <- list(DataSetName1 = "path/fileName1.csv", DataSetName2 = "path/fileName2.csv")

for (ds in names(raw_data)) {
  cd <- read.csv(raw_data[[ds]], header=T, row.names = 1)
  sg <- as.factor(rep("all", ncol(cd)))
  names(sg) <- colnames(cd)
  cd<- cd[rowSums(cd) > 0,] #sanity check to remove non-expressed genes
  #data set should already be filtered!!!
  
  #error models are simply loaded.
  load(sprintf("%s/errormodels_%s.rda",DATAFOLDER, ds))
  cd <- cd[,rownames(o.ifm)] #in case filtering has been performed on error model fit.
  
  #compute cell wise gene expression posteriors
  o.prior <- scde.expression.prior(models=o.ifm,counts=cd,length.out=400,show.plot=F)
  posteriors <- scde.posteriors(o.ifm, cd, o.prior, n.cores=n.cores, return.individual.posterior.modes=T, return.individual.posteriors=T)
  
  #determine posterior modes
  grid <- as.numeric(colnames(posteriors$jp))
  jp_modes <- apply(posteriors$jp,1,function(x) sum(x * grid))
  jp_modes <- sapply(jp_modes, function(x) which.min(abs(x - grid)))
  
  #perform integration along posteriors to compute posterior odds ratio
  normalized <- do.call(cbind, mclapply(colnames(cd), function(cell) {
    sapply(rownames(posteriors$jp), function(gene) {
      x <- jp_modes[gene]
      probability_higher <- sum(exp(posteriors$post[[cell]][gene,x:ncol(posteriors$post[[cell]])] ))
      probability_lower <- sum(exp(posteriors$post[[cell]][gene,1:x] ))
      probability_higher/probability_lower
      
    })
  }, mc.cores = n.cores))
  
  colnames(normalized) <- colnames(cd)
  normalized <- log2(normalized)
  normalized[normalized == Inf] <- max(normalized[normalized != Inf])
  
  save(normalized, file=sprintf("%s/POR_%s.rda",DATAFOLDER, ds))
}