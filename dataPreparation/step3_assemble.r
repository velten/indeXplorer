#1. Package requiremnts
require(flowCore)
require(biomaRt) #in case your data is not from human, adjust lines 89ff.
require(GO.db)
require(tsne)

#2. specify the folder that processed data should be written to
#   This should be the same folder specified in global.R for use with indeXplorer
DATAFOLDER <- "~/Documents/StaffScientist/Teaching/EMBO_sincell/PracticalPreProcessing/final/"

#3. specfiy the species (one of: human, mouse, fly)
SPECIES <- "human"

#4. Raw and pre-processed data files
#   CSV files should be separated by comma, contain colnames in the first row and leave the top-left cell empty. Put differently, the R command
#   data <- read.csv("filename", header=T)
#   needs to yield a valid data frame with rownames and colnames as described above.
#Choose Data Set Names to your liking!

dataSets <- list(
  DataSetName1 = list(
    #set true for single cell transcriptomic data sets and false for other (e.g. functional) data sets
    isTranscriptome = T,
    #For each transcriptomic data set, a csv file of raw read counts with
    #    columns representing single cells and rows representing genes. 
    #    The data set should already be filtered according to your QC standards. 
    #    The rownames of the data should be ENSEMBL ids. 
    #    The colnames of the data can be any unique identifier, for example plate<plateid>_<row>_<column> (plate4_A_7 etc). 
    #    Spike-in data is not used by indeXplorer and should be excluded already.
    #For each non-transcriptomic, e.g. functional data set, a csv file with
    #    columns representing single-cell features (e.g. total number of cells in colony)
    #    and rows representing single cells. The colnames of the data should be meaningful 
    #    descriptors of the features, rownames should be cell identifiers e.g. plate<plateid>_<row>_<column>
    raw = "~/Documents/StaffScientist/Teaching/EMBO_sincell/PracticalPreProcessing/data_filtered.csv",
    #Highly recommended: For each data set, a csv file of flow cytometry data for the single-sorted cells. 
    #    Rownames should be identifiers, e.g. plate<plateid>_<row>_<column>. 
    #    Column names should be meaningful (for example, CD34), and will be prepended FACS_ in indeXplorer.
    #In case you wish to use indeXplorer without indeces, set to NULL
    indeces = NULL,
    #Optional: For each data set, A FCS file containing all (background) FACS data. 
    #     We recommend some pre-gating (e.g. in our data set, we already excluded lineage positive cells) 
    #     and subsampling to ~100.000 events, as plotting can be quite slow otherwise.
    #In case you do not want to include background FACS data, set to NULL
    background = NULL,
    #in case the fcs the file has illegible column names, specify the column names to use in indeXplorer here
    #these should be the same as in the index file
    #In case column names in the fcs file and in the index file are already the same, set to NULL
    facs_column_names = NULL,
    #Optional: Any number of processed views on your data that you want to make accessible through indeXplorer. 
    #      a t-SNE and a PCA are automatically pre-computed
    #      Rownames should be unique cell identifers e.g. plate<plateid>_<row>_<column>. Column names should be meaningful (e.g. tSNE_x).
    #Set to NULL if you do not want to include processed views on the data
    views = NULL,
    
    
    #Finally, for single cell transcriptoimic data, normalized and scaled gene expression data
    #See readme, you can either compute posterior odds ratios using step2_optional_POR.r (adjust Data Set Name!):
    normalized = sprintf("%s/POR_%s.rda", DATAFOLDER, "DataSetName1")
    #Or you can include your own csv file of normalized gene expression
    #     The rownames of the data should be ENSEMBL ids. 
    #     The colnames of the data should be plate<plateid>_<row>_<column> (for example, plate4_A_7).
    #normalized = "/path/to/normalized/data.csv"
    #Set to NULL only if isTranscriptome is set to F
    #normalized = NULL
    
  )
  #create multiple named entries in this list according to the scheme above in case you have several data sets!
)

#4. Start loading data and creating the data files needed by indeXplorer.
# !! The script assumes that all input data files are exactly in the format
# !! described above; if you are unsure, run the part below line-by-line

#Set up objects needed by indeXplorer
univariates <- list() #stores all data that users can access through indexplorer
allgenes <- list() #stores names of all genes included in the data
isTranscriptome <- c()
complete_facs <- list() #stores background FACS data
logicleTrans <- list() #stores biexponential / logicle transforms for each FACS marker
# So that these do not need to be computed on the fly

for (ds in names(dataSets)) {
  #1a. load processed and raw gene expression data (for transcriptome data sets)
  isTranscriptome[ds] <- dataSets[[ds]]$isTranscriptome
  if (dataSets[[ds]]$isTranscriptome) { 
    CELLS <- read.csv(dataSets[[ds]]$raw, header= T, row.names=1) 
    if(grepl("rda$",dataSets[[ds]]$normalized, ignore.case = T)) {
      load(dataSets[[ds]]$normalized)
    } else {
      normalized <- read.csv(dataSets[[ds]]$raw, header= T) 
    }
    CELLS <- CELLS[,colnames(normalized)] #make sure same cells are contained in both data sets ( if processing included additional filter steps)
    
  } else {
    #1b. load processed data (for other data sets)
    normalized <- read.csv(dataSets[[ds]]$raw, header= T, row.names = 1) 
  }
  
  #2. Create mapping: ENSEMBL to HGNC, and rename genes according to indeXplorer format
  #   Then, save raw read count data (only used in differential expression module and loaded during indexplorer startup)
  
  if (!is.null(dataSets[[ds]]$indeces)) {
    indeces <-  read.csv(dataSets[[ds]]$indeces, header= T) 
    #in case numbers are used as identifiers, prepend X
    rownames(indeces)<-sapply(rownames(indeces), function(x) if (!is.na(as.numeric(x))) paste0("X",x) else x)
    usecells <- intersect(rownames(indeces), colnames(normalized))
    if ( length(usecells) < ncol(normalized)) {
      notused <- colnames(normalized)[! colnames(normalized) %in% usecells]
      warning("Index values are missing for some cells. These cells are removed from all datasets", paste0(notused,","))

    }
    normalized <- normalized[,usecells]
    CELLS <- CELLS[,usecells]
    indeces <- indeces[usecells,] #remove cells not included in the final transcriptome/function data set, make sure same row order is used
    colnames(indeces) <- paste0("FACS_",colnames(indeces))
  }
  
  
  if (dataSets[[ds]]$isTranscriptome) {   
    # indeXplorer uses the following gene name convention
    # human-readable (ENSEMBL Gene ID), for example CCND1 (ENSG00000110092)
    #  ADJUST FOR NON-HUMAN DATA! e.g. for mouse map to MGI
    dataset <- switch(SPECIES, human = "hsapiens_gene_ensembl",
                      mouse = "mmusculus_gene_ensembl",
                      fly = "dmelanogaster_gene_ensembl")
    mart = useMart("ENSEMBL_MART_ENSEMBL",dataset=dataset, host = "www.ensembl.org")
    genename <- switch(SPECIES, human = "hgnc_symbol",
                       mouse = "mgi_symbol",
                       fly = "external_gene_name")
    martf <- getBM(attributes = c("ensembl_gene_id",genename,"entrezgene") , filters = "ensembl_gene_id", values = rownames(CELLS),mart=mart)
    ensembl <- martf$ensembl_gene_id
    entrez <- martf$entrezgene; names(entrez) <- ensembl
    alias <- martf[,2]; names(alias) <- ensembl
    
    oldlabels <- rownames(CELLS)
    newlabels <- sapply(rownames(CELLS), function(x){
      if (length(alias[x]) > 1) cat(x, "\n")
      if(!is.na(alias[x]) & !alias[x]=="") paste(alias[x] , " (", x , ")", sep="") else paste("? (", x , ")", sep="")
    })
    
    rownames(CELLS) <- newlabels
    rownames(normalized) <- newlabels
    allgenes[[ds]] <- newlabels
    
    save(CELLS, file = sprintf("%s/%s.rda",DATAFOLDER,ds))
    
  }
  
  univariates[[ds]] <-t(normalized)
  
  #3. load index facs data
  if (!is.null(dataSets[[ds]]$indeces)) univariates[[ds]] <- cbind( indeces, univariates[[ds]] )
  
  
  
  pca <- prcomp(t(normalized))
  tsne <- tsne(pca$x[,1:10])
  autoviews <- data.frame(pca$x[,1:6], tsne_x = tsne[,1], tsne_y = tsne[,2], genesObserved = apply(CELLS>=5, 2,sum), rawReadCount = apply(CELLS,2,sum))
  univariates[[ds]] <- cbind( autoviews, univariates[[ds]] )
  #4. load precomputed "views" on the data
  if (!is.null(dataSets[[ds]]$views)) {
    views <-  read.csv(dataSets[[ds]]$views, header= T) 
    views <- views[colnames(normalized),] #remove cells not included in the final transcriptome/function data set, make sure same row order is used
    univariates[[ds]] <- cbind( univariates[[ds]], views )
  }
  
  #5. assemble everything into one big flow Frame
  univariates[[ds]] <- flowFrame(as.matrix(univariates[[ds]]))
  
  #6. Load background FACS data
  if (!is.null(dataSets[[ds]]$background)) {
    complete_facs[[ds]] <- read.FCS(dataSets[[ds]]$background)
    complete_facs[[ds]] <- complete_facs[[ds]]@exprs
    if (!is.null(dataSets[[ds]]$facs_column_names)) { 
      colnames(complete_facs[[ds]]) <- dataSets[[ds]]$facs_column_names
    } else if (!identical( colnames(complete_facs[[ds]]), colnames(indeces))) {
      stop("Error: Column names of index file and FCS file need to be identical, or updated FCS file column names need to be explicitly provided")
    }
    complete_facs[[ds]] <- flowFrame(as.matrix(complete_facs[[ds]])) 
    logicleTrans[[ds]] <- estimateLogicle(complete_facs[[ds]], colnames(complete_facs[[ds]]@exprs))
  } else if (!is.null(dataSets[[ds]]$indeces)) {
    logicleTrans[[ds]] <- estimateLogicle(univariates[[ds]], colnames(indeces))
  }
}

#save everything
save(univariates, martf, allgenes, complete_facs, logicleTrans, isTranscriptome, file=file.path(DATAFOLDER, "MASTER_ALL.rda"))

#create some Gene Ontology objects 
cc <- as.list(GOCCCHILDREN)
bp <- as.list(GOBPCHILDREN)
mf <- as.list(GOMFCHILDREN)
GOCHILDREN <- c(bp,cc,mf)
xx <- as.list(GOTERM)
GOdescription <- data.frame(id = sapply(xx, function(x) x@GOID),
                            term =sapply(xx, function(x) x@Term),
                            ont =sapply(xx, function(x) x@Ontology))

save(GOCHILDREN, file=file.path(DATAFOLDER, "GOchildren.rda"))
save(GOdescription, file=file.path(DATAFOLDER, "GOterms.RData"))