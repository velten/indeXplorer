

####plot type #####
output$whattoplot <- renderUI({

    
    list( selectInput("pltType", "Dataset to plot", choices = names(univariates)),
          textInput("onxaxis_free", "X-Axis:",value=""), 
          textInput("onyaxis_free", "Y-Axis:",value=""),
          helpText("To search for a marker or gene, type any substring. FACS markers start with", tags$em("FACS_"), ". To perform a principle component analysis, select", tags$em("^PC"), " to perform it on all genes. If you select a gene list (starting with", tags$em("GL_"), "), you can project its expression via PCA. To view PCA diagnositcs, use the PCA module.")
    )
})

#render the option boxes where the user can select the variable that mataches his search string
#### search functions ####
output$search <- renderUI({
  
    if (is.null(input$onxaxis_free) | is.null(input$onyaxis_free)) {
      toshow_x <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"x"] 
      toshow_y <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"y"] 
      #  p("Please use the free text boxes to look for variables, e.g. FACS_cd34 for a FACS marker, GO:123456 for a gene ontology, or a gene name / ENSEMBL id for a gene")
      #} else if (nchar(input$onxaxis_free) <3 & nchar(input$onyaxis_free)<3) {
      #  p("Please use the free text boxes to look for variables, e.g. FACS_cd34 for a FACS marker, GO:123456 for a gene ontology, or a gene name / ENSEMBL id for a gene")
    } else {
      choices <- colnames(univariates[[input$pltType]])
      if (isTranscriptome[input$pltType]) choices <- c(choices, paste("GL", as.vector(sapply(names(globalData$geneLists), function(x) paste(x, c("PC 1", "PC 2"), sep=" "))),sep="_"))
      if (nchar(input$onxaxis_free) <3 & isTranscriptome[input$pltType]) toshow_x <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"x"] else toshow_x <- choices[grep(input$onxaxis_free,choices,ignore.case=T)]
      if (nchar(input$onyaxis_free) <3 & isTranscriptome[input$pltType]) toshow_y <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"y"] else toshow_y <- choices[grep(input$onyaxis_free,choices,ignore.case=T)]
      
     
    
        #print(globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"x"])
        list(
          selectInput("onxaxis", label = "X-Axis", choices = toshow_x, selected =  globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"x"]),
          selectInput("onyaxis", label = "Y-Axis", choices = toshow_y, selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"y"]),
          checkboxInput("displayContours",label = "Display density plot of all FACS events in background (will make plotting slower)",value =  globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"showDensity"])
        )
        

      
      
    }      
  
})

output$scale <- renderUI({
 # browser()
  if(is.null(input$onxaxis)) choicex <- c("linear","log","asinh","logicle") -> choicey else {
    
    if (substr(input$onxaxis,1,5) != "FACS_") choicex <- "linear" else choicex <-  c("linear","log","asinh","logicle")
    if (substr(input$onyaxis,1,5) != "FACS_") choicey <- "linear" else choicey <-  c("linear","log","asinh","logicle")
  }
  list(
    selectInput("xscale", label = "X-Axis Transform", choices = choicex, selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"x_scale"]),
    selectInput("yscale", label = "Y-Axis Transform", choices =choicey, selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"y_scale"])
  )
  
})
