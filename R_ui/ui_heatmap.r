require(pheatmap)
require(RColorBrewer)

output$HeatmapPopulationSelect <- renderUI({
  list(
    selectInput("HeatmapPopulation","Please select any number of cell populations to display",choices=colnames(globalData$Populations[[globalData$activeDataSet]]), selected ="all",multiple=T),
    selectInput("HeatmapLabelPopulation","Select any number of populations to use for assigning cell labels, or leave empty to use a unique identifier (of type plate1_B_5) for each cell",choices=colnames(globalData$Populations[[globalData$activeDataSet]]), selected ="",multiple=T),
    numericInput("Heatmap.CutTree","Clusters to cut tree into", value=1, min=1,max=25, step=1)
    
  )
  
})

output$HeatmapGeneSelect <- renderUI({
  list(#checkboxInput("Heatmap.brennecke", "Use only genes with large vairability (Brennecke test)", value=T),
      numericInput("Heatmap.noGenes", "Number of most variable genes to display:",value=100, min=10,max=500, step=1),
       tags$em("or"),
        selectInput("HeatmapGeneList", "Select a gene list to display", choices=c("None",names(globalData$geneLists)),selected="None"),
       tags$em("and"),
       textInput("HeatmapGenes", "Type any number of gene symbols, entrez/ensembl IDs or GO IDs, separated by spaces. You can also include FACS markers by typing e.g. FACS_c34."),
       radioButtons("Heatmap.ClusterAll", "Cluster on",choices=c("most variable 1000 genes","displayed genes"),selected="most variable 1000 genes", inline=T)
  )
})

output$Heatmap <- renderPlot({
  
  if (input$HeatmapMake[1] > 0 ) {
 #   input$HeatmapMake[1] -> globalData$heatmapMakeCall
    #choose genes
    print("heatmap")
    genes <- c()
    if(isolate(input$HeatmapGenes) != "") {
      split <- strsplit(isolate(input$HeatmapGenes), split="[[:space:]]+")
      #genes <- unlist(sapply(split[[1]], checkInput))
      genes <- unlist(sapply(split[[1]], checkInput,martf=martf[paste0(martf[,2], " (", martf$ensembl_gene_id,")") %in% allgenes[[globalData$activeDataSet]], ], uv = univariates[[globalData$activeDataSet]] ))
    } 
    gl <- isolate(globalData$geneLists)
    if (isolate(input$HeatmapGeneList) != "None") {
      genes <- c(genes, gl[[isolate(input$HeatmapGeneList)]])
    }
    
    if(length(genes) == 0) genes <- allgenes[[globalData$activeDataSet]]
    
    
    populations <- isolate(input$HeatmapPopulation)
    
    population_labels <- isolate(input$HeatmapLabelPopulation)
    population_df <- isolate(globalData$Populations[[globalData$activeDataSet]])
    clusterAll <- isolate(input$Heatmap.ClusterAll) == "most variable 1000 genes"
    clusterMethod <- isolate(input$Heatmap.method)
    distanceCells <- isolate(input$Heatmap.distCells)
    distanceGenes <- isolate(input$Heatmap.distGenes)
    n.correlation=isolate(input$Heatmap.nCor)
    mode.correlation=isolate(input$Heatmap.modeCor)
    method.correlation=isolate(input$methodCor)
    cut = isolate(input$Heatmap.CutTree)
    #significant = isolate(input$Heatmap.brennecke)
    
    
    
    populationDefinition <- population_df[,populations]
    if (length(populations) == 1) useCells <- populationDefinition else useCells <- apply(populationDefinition, 1, max) == 1
    globalData$HeatmapPopulation <- useCells
    #assign lables
    if (is.null(population_labels)) labels <- rownames(univariates[[globalData$activeDataSet]]@exprs) else if (length(population_labels) == 1) {
      labels <- ifelse(population_df[,population_labels], population_labels, "other") 
    } else {
      labels <- and_connect(population_df[,population_labels])
    }
    
    correlationMode <- length(genes) == 1
   

    
    #if (!correlationMode){
    numberOfGenes <- isolate(input$Heatmap.noGenes)
    isGene <- grepl("ENSG|ENSMUSG|FBgn",colnames(univariates[[globalData$activeDataSet]]@exprs))
    vars <- apply(univariates[[globalData$activeDataSet]]@exprs[useCells,isGene], 2, var)
    vars <- vars[order(vars, decreasing=T)]
      if(length(genes) == length(allgenes[[ globalData$activeDataSet ]]) ) genes <- names(vars)[1:numberOfGenes]
     # }
    
    #use only genes that are significant in the brennecke method?
    #if (significant) {
    #  if (length(genes) > 1) genes <- genes[genes%in% brennecke_sig | grepl("FACS",genes)]
    #} else 
    genes <- genes[genes %in% allgenes[[globalData$activeDataSet]]]
    #If only one gene is supplied, look for well correlated genes
    if (correlationMode) {
      #if(significant) use <- brennecke_sig else 
      #use <- names(which(apply(CELLS,1,mean) > 50))
      #use <- names(which(apply(univariates[[globalData$activeDataSet]]@exprs[,allgenes[[globalData$activeDataSet]]]>0,2,sum) > 50))
      use <- names(vars[1:1000])
      print(head(use))
      cors <- apply(univariates[[globalData$activeDataSet]]@exprs[useCells,use], 2, function(bait, target) {
        cor(bait, target, method=method.correlation)
      }, target = univariates[[globalData$activeDataSet]]@exprs[useCells,genes])
      res <- switch(mode.correlation, 
                    absolute = cors[order(abs(cors),decreasing=T)][1:n.correlation],
                    positive = cors[order(cors,decreasing=T)][1:n.correlation],
                    negative = cors[order(cors,decreasing=F)][1:n.correlation])
      genes <- c(names(res), genes)
      
    } 
    
    genes <- unique(genes)
    #if there are entries which are not genes, rescale to the variance of the gene with the max. variance
    
    
    if(length(genes) < 2) return(NULL) else {
      
      sCELLS <- t(univariates[[globalData$activeDataSet]]@exprs[useCells,genes])
      labels <- labels[useCells]
      if (is.null(population_labels)) anno_df <- NA else anno_df <- data.frame(class = labels, row.names=colnames(sCELLS))
      
      
      #rescale FACS scores
      affected <- grepl("FACS",genes)
      if (sum(affected) > 0){
        if (sum(affected) == length(genes)) maxvar <- 1 else maxvar <- max(apply(sCELLS[!affected,],1,sd))
        if (sum(affected)==1) {
          sCELLS[affected, ] <- transform(sCELLS[affected], "biexponential")
          sCELLS[affected, ] <-  (sCELLS[affected, ]-mean(sCELLS[affected, ])) / sd(sCELLS[affected, ]) * maxvar
        }
        else {
          sCELLS[affected, ] <- apply(sCELLS[affected,], 1, transform, how = "biexponential")
          sCELLS[affected, ] <- apply(sCELLS[affected,], 1,function(x) (x-mean(x)) / sd(x) * maxvar)
        }
      }
      
      #possibility 1 to perform a hierarchical clustering: the tried & tested one - significant genes, ward
      #   dist_cells <- dist(t(sCELLS[genes,]))^2 
      #   dist_genes <- dist(sCELLS[genes,])^2
      #   cellcluster <- hclust(dist_cells, method="ward")
      #   genecluster <- hclust(dist_genes, method="ward")
      
      #compute distances
      
#       if(clusterAll & significant) {
#         if(distanceCells == "Euclidean") {
#           dist_cells <- dist(univariates[[globalData$activeDataSet]]@exprs[useCells,brennecke_sig])
#         } else {
#           dist_cells <- as.dist(1 - cor(t(univariates[[globalData$activeDataSet]]@exprs[useCells,brennecke_sig])))
#         }
#       } else 
      if (clusterAll) {
        if(distanceCells == "Euclidean") {
          dist_cells <- dist(univariates[[globalData$activeDataSet]]@exprs[useCells,names(vars)[1:1000]])
          #writeLines(names(vars)[1:1000], con="test.txt")
        } else {
          dist_cells <- as.dist(1 - cor(t(univariates[[globalData$activeDataSet]]@exprs[useCells,genes])))
        }
      } else {
        if(distanceCells == "Euclidean") {
          dist_cells <- dist(t(sCELLS))
        } else {
          dist_cells <- as.dist(1 - cor(sCELLS))
        }
      }
      
      if(distanceGenes == "Euclidean") {
        dist_genes <- dist(sCELLS)
      } else {
        dist_genes <- as.dist(1 - cor(t(sCELLS)))
      }
            
      # a smart colorscale. Step = 1bit BF
      min <- min(sCELLS)
      max <- max(sCELLS)
      below  <- min +5#number of steps below -5
      if (below < 0) below <- abs(below) else below <- 0
      above <- max- 5 #number of steps above 15
      if (above < 0) above <-0
      ramp <- colorRampPalette(rev(brewer.pal(n = 7, name ="RdYlBu")))(20)
      colors <- c(rep(ramp[1], below), ramp, rep(ramp[20], above))
      
      callback = function(hc, mat){
        sv = svd(t(mat))$v[,1]
        dend = reorder(as.dendrogram(hc), wts = sv)
        as.hclust(dend)
      }
      #browser()
      test <- pheatmap(sCELLS, color = colors, cluster_rows = T, cluster_cols = T, clustering_distance_cols = dist_cells, clustering_distance_rows = dist_genes, annotation_col = anno_df, show_rownames=T,show_colnames=F, clustering_method=clusterMethod,clustering_callback=callback, cutree_cols = cut,fontsize_row=7, border_color = NA)
    }
    globalData$dendrogram <- test$tree_col
    globalData$heatmapDims <- dim(sCELLS)
    #if (is.null(globalData$dendrogram)) showshinyalert(session,"HMalert",HTMLtext="Less than 2 genes from your list match the criteria, e.g. no valid gene selected or all genes not significantly variable",styleclass="warning")
    #plot(1:10)
    
   #
 }
  
}, width = function() min(globalData$heatmapDims[2] * 3 + 350,1400), height = function() min(globalData$heatmapDims[1] * 4 + 200,1200))

createPopulations <- observe({
if (globalData$heatmapPopCall < input$HeatmapPop[1]) {
globalData$heatmapPopCall <- input$HeatmapPop[1]
dendr <- isolate(globalData$dendrogram)
cut <- isolate(input$Heatmap.CutTree)
  if (!is.null(dendr)) {
    cell_cluster <- cutree(dendr, k = cut)
    pop <- do.call(cbind, lapply(1:max(cell_cluster), function(x) x == cell_cluster))
    populations <- do.call(cbind, lapply(1:max(cell_cluster), function(x) rep(F, length(globalData$HeatmapPopulation)) ))
    populations[globalData$HeatmapPopulation] <- pop
    existing <- colnames(globalData$Populations[[globalData$activeDataSet]])
    existing <- existing[grepl("Heatmap",existing)]
    if (length(existing) > 0) {
      split <- sapply(strsplit(existing, split="-"), function(x) x[1])
      print(as.integer(substr(split,8,100 )))
      id <- max(as.integer(substr(split,8,100 ))) + 1
    } else id <- 1
    
    colnames(populations) <- paste("Heatmap",id,"-",1:max(cell_cluster),sep="")
    
    globalData$Populations[[globalData$activeDataSet]] <- cbind(globalData$Populations[[globalData$activeDataSet]], populations)
    #globadData$Gates
  } 
}
})
