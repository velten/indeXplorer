
###gate selector ####
output$gateSelector <- renderUI({
   pltype<- input$pltType
  
  list( textInput("gateName", label = "Name of new Gate", value="NewGate"))
#        actionButton("importGates", btnlbl))
})


###population selector####
output$groupSelection <- renderUI({
  selection <- strsplit(globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"todisplay"],"_")[[1]]
  
  pltype<- input$pltType
  lbl <- paste("Select Gates for",pltype,"that apply")
  
  list( 
    #checkboxInput("displayFACS", label = "Display all FACSed cells on current plot (Note: Will only work on plots with FACS markers on both axis and biexponential scaling)",value=FALSE),
    checkboxGroupInput("populations2show", label =lbl, choices = c("all",getGateNames(pltype)), selected = selection),
    radioButtons("gatingMode", label ="If multiple gates are selected, combine by", choices = c("AND","OR"), selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"gatingMode"])
  )
})

output$hideSelection <- renderUI({
  use <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"tohide"]
  if(use=="") selection <- "" else selection <- strsplit(use,"_")[[1]]
  pltype<- input$pltType
  #browser()
  list(
    helpText("In case you wish to display all background events, but only some single cell transcriptomics or single cell culture events, you can here select the populations to hide from display"),
    checkboxGroupInput("populations2hide", label ="Select Populations to hide from display", choices = colnames(globalData$Populations[[pltype]])[-1], selected = selection)
    
  )
  
})
