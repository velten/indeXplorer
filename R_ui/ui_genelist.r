output$geneListCreate <- renderUI({
if(input$geneListType == "File Upload") list(
      fileInput("uploadGeneList", "Upload Gene List", accept="text/plain")
      ) else if (input$geneListType == "Set operation") list(
       selectInput("GLSetType", label="Type of set operation", c("Intersection", "Union"), "Intersection"),
       selectInput("GLSetMembers", label="Select any number of gene Lists", names(globalData$geneLists), multiple=T)
      ) else if (input$geneListType == "Import GO") {
          textInput("GLGOid", label= "Free search of GO terms and IDs") 
        } else if (input$geneListType == "Literature") {
          list(selectInput("GLResource", label="Select resource", choice = resources$name, selected = "Chen et al 2014")
          )
        }
 
})

output$GLgoSelect <- renderUI({
  if (input$geneListType == "File Upload") {
    return(helpText("Text files with one entry per line. Lines can contain: ENSEMBL ids, entrez gene ids, HGNC gene names, and/or GO terms. HGNC gene names can end in an asterisk; e.g. CD* will create a gene list of all cluster of differentiation genes.")) 
  }
  else if (input$geneListType == "Literature") {
    text <- resources$info[resources$name == input$GLResource]
    pmid <- resources$pubmed[resources$name == input$GLResource]
    type <- resources$type[resources$name == input$GLResource]
    species <- resources$species[resources$name == input$GLResource]
    return (
      p(text, tags$b(type), tags$b(species), a("pubmed", href = paste("http://www.ncbi.nlm.nih.gov/pubmed/",pmid,sep=""), target="_blank"))
    )
  } else if (input$geneListType != "Import GO" | is.null(input$GLGOid)) return() else if (nchar(input$GLGOid) > 3) {
    toshow <- grepl(input$GLGOid, GOdescription$id,ignore.case=T) | grepl(input$GLGOid, GOdescription$term,ignore.case=T)
    selectInput("GLGOselect",label="Select GO ID", choices= paste(GOdescription$term[toshow], " (", GOdescription$id[toshow], ")", sep=""), width = '100%')
  }
})


output$geneListSelector <- renderUI({
  selectInput("GL2display", label="Select a gene List to display", names(globalData$geneLists), selected = ifelse(is.null(input$GL2display), "",input$GL2display ))
})

output$geneListDisplay <- renderUI({

    if (is.null(input$GL2display)) helpText("No gene List selected") else if (length(globalData$geneLists[[input$GL2display]]) > 500) {
            fluidRow(tags$b("This gene list is too long to be displayed in the browser"))
    } else {
      geneListDisplay <- lapply(globalData$geneLists[[input$GL2display]], function(x){
                                  geneName <- strsplit(x,"[\\(\\)]")[[1]][1]
                                  tags$li(x, tags$a("genecards",href=paste("http://www.genecards.org/cgi-bin/carddisp.pl?gene=",geneName,sep=""), target="_blank"), 
                                              tags$a("pubmed",href=paste("http://www.ncbi.nlm.nih.gov/pubmed?term=",geneName,"%20hematopo*",sep=""), target = "_blank")
                                  , id = x, oncontextmenu= 'passID(this.id); return false;')
      })
      
      nitems <- length(geneListDisplay)
      
      
        if (nitems < 10) fluidRow(tags$b("Right click to remove from the selected gene list!"), tags$ul( geneListDisplay )) else {      
      fluidRow(column(width = 4, 
                     tags$b("Right click to remove from the selected gene list!"),
                     tags$ul( geneListDisplay[1:floor(nitems/3)] )),
              column(width =4 , tags$ul( geneListDisplay[ceiling(nitems/3):floor(2*nitems/3)] )),
              column(width =4 , tags$ul( geneListDisplay[ceiling(2*nitems/3):nitems]  )) 
      )
        
      } 
    }
    
})

output$GLdownload <- downloadHandler(filename = function() {
  paste(input$GL2display, "txt",sep=".")
}, content = function(file) {
  current <- isolate(globalData$geneLists[[input$GL2display]])
  ensembl <- sapply(strsplit(current,"[\\(\\)]"), function(x) x[2])
  writeLines(ensembl,file)
})