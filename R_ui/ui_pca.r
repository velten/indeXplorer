output$PCAIntroduction <- renderUI({
  #recentpcs <- names(globalData$projections)
  #pcdisplay <- lapply(recentpcs, function(x) {
  #  tags$a(x, href="#", id = x, onclick='quickPCA(this.id); return false;')
  #  })
  
  list(
    tags$p(tags$b("This module provides the ability to perform principle component analysis."), "If you are simply interested in plottig PCA scores plots, you can also use the", tags$em("Scatter plots"), "Module."),
    #tags$p("You can select one of the PCAs you have performed in the scatter plots module to quickly view the corresponding diagnostics:"),
    #pcdisplay,
    tags$hr()
  )
})

output$PCAPopulationSelect <- renderUI({
list(
    selectInput("PCAPopulation","Please select any number of cell populations to display",choices=colnames(globalData$Populations[[globalData$activeDataSet]]), selected ="all",multiple=T),
    selectInput("PCAHighlightPopulation","Please select any number of cell populations to highlight on the scores plot",choices=colnames(globalData$Populations[[globalData$activeDataSet]]), selected ="all",multiple=T)
)
    
})

output$PCAGeneListSelect <- renderUI({
  list(selectInput("PCAGeneList", "Select a gene list to perform PCA on", choices=c("All genes",names(globalData$geneLists)),selected="All genes"),
       selectInput("PCAHighlightGeneList", "Select a gene list to highlight on the loadings plot", choices=c("None",names(globalData$geneLists)),selected="None")
  )
})

output$LoadingsTable <- renderDataTable({
  torun <- 8
  print(torun)
  data.frame(gene = rownames(pca$loadings), pca$loadings[,1:torun])
})

