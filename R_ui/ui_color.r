#### plot color type ######
output$plotColor <- renderUI({
  #print("UI selector")
  if(isTranscriptome[input$pltType]) choices <- c("univariate","population","gene list","PCA","none") else  choices <- c("univariate","population","none")
  selectInput("pltColorType", label = "Select type of color display", choices, selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_type"])
})

#### plot color selection ######
output$ColorSelection <- renderUI({
  #print("UI selector")
  if (!is.null(input$pltColorType)){
    pltype<- input$pltType
    if (input$pltColorType == "none") return() else if (input$pltColorType == "univariate") {
      textInput("oncaxis_free", "To search for a marker or gene/property, type any substring.",value="")
    } else if (input$pltColorType == "population") {
      if (ncol(globalData$Populations[[pltype]]) > 1) {
        if (globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_type"] == "population") selection <- strsplit(globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_variable"],"_")[[1]] else selection <- NULL
        checkboxGroupInput("ColorPopulations", label = "Select any number of populations to highlight.", choices=colnames(globalData$Populations[[pltype]])[-1], selected=selection)
      } else {
        helpText("No populations created yet")
      }
    } else if (input$pltColorType == "gene list") {
      if (length(globalData$geneLists) == 0) helpText("Create Gene Lists in the Gene list panel first") else {
      list( 
        selectInput("ColorGeneSet", label = "Select Gene List", selected="", choices=names(globalData$geneLists)),
      selectInput("ColorGeneMapping", label = "Select Mapping Type", selected = "PCA projection",
                  choices = c("PCA projection", "Sum of raw expression values"))
      )
      }
    } else if (input$pltColorType == "PCA") {
      list(helpText("PCA is performed on all genes and the cells that are actually displayed in the plot. To use only a subset of all genes for PCA, select \"gene list\" and method \"PCA projection\". For diagnostics, use the PCA module."),
           selectInput("ColorPCA",label = "Select Principle Component to Plot", selected=1,choices=1:9))
    }else {
      helpText("Not implemented yet.")
    }}
})

output$ColorSearch <- renderUI({
  if (!is.null(input$pltColorType)){
    if (input$pltColorType == "univariate"){
      pltype<- input$pltType
      if (is.null(input$oncaxis_free)) {
        if ( globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_type"] == "univariate"  ) toshow_c <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_variable"] else toshow_c <- ""
      } else {
        if (nchar(input$oncaxis_free) <3 & isTranscriptome[pltype]) {
          if ( globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_type"] == "univariate"  ) toshow_c <- globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_variable"] else toshow_c <- ""
        } else toshow_c <- colnames(univariates[[pltype]])[grep(input$oncaxis_free,colnames(univariates[[pltype]]),ignore.case=T)]
        
        list(
          selectInput("oncaxis", label = "Gene/Marker", choices = toshow_c, selected =  globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_variable"]),
          selectInput("cscale", label = "Transform", choices = c("linear","log","quantile","logicle"), selected = globalData$plots[globalData$plots[,"id"] == globalData$activePlot,"color_scale"])
        )
        
        
      }    } else if (input$pltColorType == "gene list") {
        switch(input$ColorGeneMapping,
               `Quantile Enrichment` = list(tags$p("Computes enrichment of the gene set in the top/bottom 10% of differentially regulated genes."), 
                                            tags$p("For each gene, take the posterior probability that its expression is higher (or lower) than the population mean. This quantity is estimated using the methodology described by Kharchenko et al. For each cell, arrange all genes by that probability and check for the enrichment in the top/bottom 10%. May not beas neat as the enrichment scores but computes faster.",style = "font-size: 70%")),
               `PCA projection` = list(tags$p("Project gene expression levels from the gene set on a single dimension, plot that dimension. To view PCA diagnostics, use the PCA module.")),
               `Sum of raw expression values` = list(tags$p("Sum of normalized, scaled raw count values."))
               
               )
      } else if (input$pltColorType == "clustering") {
        switch(input$ColorClustSelect,
               `BHC of gene expression` = list(tags$p("Performs Bayesian Hierarchical Clustering and plots cluster memberships as colors."),
                                               tags$p("BHC works like normal agglomerative clustering, but merges subtrees based on the probability that their leaves actually arise from the same distribution. If so, no further splits are performed. I used",
                                                      tags$a("the R/BHC package.", href = "http://dx.doi.org/10.1186/1471-2105-10-242", target="_blank"), style = "font-size: 70%"))
               
        )
      }
  
  }
})


