output$KharchenkoPopulationSelect <- renderUI({
    if (is.null(globalData$showKharchenko)) list(
      selectInput("KharchenkoPopulation1",label="Select population to test for", choices=colnames(globalData$Populations[[globalData$activeDataSet]]), multiple=F),
      selectInput("KharchenkoPopulation2",label="Select population to test against", choices=colnames(globalData$Populations[[globalData$activeDataSet]]), multiple=F),
#      textInput("KharchenkoEmail",label="E-mail address (optional)"),
      helpText('Note: Cells that are in the first population are excluded from the second population, so selecting "all" as the second population will test agains all other cells.')
    ) else list(
      selectInput("KharchenkoPopulation1",label="Select population to test for", choices=colnames(globalData$Populations[[globalData$activeDataSet]]), multiple=F,selected=globalData$showKharchenko[1]),
      selectInput("KharchenkoPopulation2",label="Select population to test against", choices=colnames(globalData$Populations[[globalData$activeDataSet]]), multiple=F,selected=globalData$showKharchenko[2]),
#      textInput("KharchenkoEmail",label="E-mail address (optional)"),
      helpText('Note: Cells that are in the first population are excluded from the second population, so selecting "all" as the second population will test agains all other cells.')
    )
})
 
output$KharchenkoGLSelect <- renderUI({

    selectInput("KharchenkoGL", label="Select a gene List to highlight, if any", c("none", names(globalData$geneLists)), selected = "none")
    
})

 
output$KharchenkoData <- renderDataTable({
  
  #if (input$KharchenkoTableToDisplay == "Genes") {
  if(nrow(kharchenko$result) == 0) NULL else {
    outdf <- isolate(kharchenko$result[order(kharchenko$result$fdr),])
    colnames(outdf) <- c("Name", "p-value", "Coefficient", "Upper CI", "Lower CI", "FDR")
    
    return(outdf)
  }
  #} else {
  #  if(nrow(kharchenko$go) == 0) NULL else { 
  #  outdf <- isolate(kharchenko$go)
  #  return(outdf)
  #}
  #}
})

output$KharchenkoDownload <- downloadHandler(filename = function() {
  #what <- ifelse(input$KharchenkoTableToDisplay == "Genes", "Genes", "GO")
  what <- "Genes"
  paste("DiffExpression", paste(input$KharchenkoPopulation1, input$KharchenkoPopulation2,sep="_"), input$KharchenkoGenes2Store, what,"csv",sep=".")
}, content = function(file) {
#  if (input$KharchenkoTableToDisplay == "Genes") {
    if(nrow(kharchenko$result) == 0) NULL else {
      
      #source("R_functions/savestate.r", local=T)
      meta <- c(sessionID = curID, 
                Populations = paste(levels(kharchenko$groups), collapse="_"), 
                ThresholdSegmentation = ifelse(input$KharchenkoThresholdSegmentation,"YES","NO"))
      meta <- paste("#", names(meta), ": ", meta, sep="")
      
      openf <- file(file, "w")
      writeLines(meta,openf)
      
      outdf <- isolate(kharchenko$result[order(kharchenko$result$fdr),])
    colnames(outdf) <- c("Name", "p-value", "Coefficient", "Upper CI", "Lower CI", "FDR")
      write.csv(outdf, file = openf, row.names =T, quote=F)
      close(openf)
   }

})
