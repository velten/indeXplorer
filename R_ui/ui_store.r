###### store session ######
output$sessionIDout <- renderPrint({
  if(input$storeSession) {
repeat {
  curID <- paste(sample(chars,size=20, replace=),collapse="")
  fname <- paste("sessions/",curID, ".r",sep="")
  if (!file.exists(fname)) break
}
    plots <- isolate(globalData$plots)
    gates <- isolate(globalData$Gates)
    populations <- isolate(globalData$Populations)
    cg <- isolate(globalData$currentGate)
    genelists <- isolate(globalData$geneLists)
    colorscores <- isolate(globalData$colorScores)
    proj <- isolate(globalData$projections)
    actds <- isolate(globalData$activeDataSet)
    dump(list=c("plots","gates","populations","cg", "genelists","colorscores",  "proj", "actds"),file=fname)

    showshinyalert(session,"alert1",HTMLtext="The current state was stored.")
    curID
  }
})
